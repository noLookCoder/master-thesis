\chapter{Wprowadzenie} \label{definitions}
\thispagestyle{chapterBeginStyle}
Początek tego rozdziału przedstawia genezę \textit{Blockchain}. Poruszony został również temat kryptowlut i~rola jaką odgrywają one~w~kontekście podejmowanego przez nas tematu. Następnie wprowadziliśmy pojęcia leżące u~podstaw opisywanej technologii. Definicje kryptograficzne opracowano w oparciu o książki \cite{applied-cryptography-handbook} oraz~\cite{modern-cryptography-introduction}. 
%JL Omówione zagadnienia wybrano w~oparciu o~książkę \cite{bitcoin-and-cryptocurrency-technologies}.


\section{Geneza Blockchain}
Zacznijmy od krótkiego wyjaśnienia czym jest Blockchain, bardziej formalną definicję tej technologii wprowadzimy w~rozdziale \ref{blockchain-definition}. 
Blockchain jest w~istocie stale powiększającą się listą rekordów, które~będziemy nazywać \textit{blokami} (ang. \textit{blocks}). Bloki te są~połączone w~łańcuch i~zabezpieczone mechanizmem kryptograficznym, który~ma~uniemożliwić ich~modyfikację. Każdy blok zawiera
hash swojego poprzednika z listy, znacznik czasu i~dane dotyczące pewnego zbioru zrealizowanych \textit{transakcji} (ang. \textit{transactions}). Łańcuch bloków zwykle osadzony jest w~sieci rozproszonej, w~której wszyscy użytkownicy mają równe uprawnienia oraz~pełny dostęp do informacji o~wykonanych transakcjach.

Pierwsza praca na~temat zabezpieczonych kryptograficznie łańcuchów danych ukazała się w~1991 roku, autorami są~Stuart Haber i~W. Scott Stornetta \cite{time-stamp-digital-document}. W~roku 1992 Bayer, Haber i~Stornetta rozszerzyli początkowy koncept o~\textit{drzewa Merkle'a} (zobacz podrozdział \ref{merkle-tree}) \cite{improving-digital-time-stamping}. Dzięki temu przechowywane dane zostały zgrupowane w~bloki, co~spowodowało wzrost wydajności odczytu informacji z~łańcucha. Pierwsza realizacja Blockchain powstała w~2009 roku jako integralna część kryptowaluty \textit{Bitcoin} \cite{bitcoin-invention}. Sposób działania i~przykłady zastosowań tego rozwiązania znajdują się w~rozdziale \ref{bitcoin}. Łańcuch bloków pełni tam rolę publicznego rejestru, który~magazynuje transakcje wykonane pomiędzy posiadaczami kryptowaluty. 

Istotnymi parametrami związanymi z~konkretną realizacją Blockchain jest stopień zaufania do użytkowników weryfikujących transakcje oraz~poziom anonimowości nabywców kryptowaluty.
Pierwotnej realizacji przyświecała idea zapewnienia możliwe dużej anonimowości przy minimalnym wzajemnym zaufaniu użytkowników. 
Przyjęte założenia wymusiły decentralizację mechanizmu weryfikacji. W~efekcie część użytkowników, nazywanych górnikami (ang. \textit{miners}), poświęca swoją moc obliczeniową na~walidację i~przyłączanie nowych bloków do~łańcucha. Za~wykonaną pracę otrzymują oni zapłatę w~kryptowalucie związanej z~systemem. Powiązanie Blockchain z~kryptowalutą i~mechanizm wynagradzania wydają się być niezbędne do~samoistnego podtrzymywania otwartych łańcuchów, dla których~weryfikacja transakcji nie jest odgórnie nadzorowana. Bez istnienia motywacji w~postaci szansy zysku, poświęcanie przez użytkowników własnych zasobów (obliczeniowych lub~jakichkolwiek innych) na~walidację transakcji wydaje się nieracjonalne. Z~tego powodu kryptowaluty odgrywają istotną rolę w~kształtowaniu się technologii Blockchain.

W~2014 roku zaczęły powstawać rejestry Blockchain umożliwiające zawieranie automatycznie wykonywanych kontraktów pomiędzy użytkownikami bez~uczestnictwa stron trzecich (ang. \textit{smart contracts}). Przykładem takiego systemu jest uruchomiona w~2015 roku kryptowaluta \textit{Ethereum} \cite{ethereum-white-paper}. Łańcuchy bloków pozwalające tworzyć \textit{inteligentne kontrakty} czasami nazywa się technologią \textit{Blockchain 2.0}. Kolejnym przedsięwzięciem wywodzącym się z~technologii Blockchain są~\textit{zdecentralizowane aplikacje} (ang. \textit{decentralized applications, DApps}), zwane \textit{Blockchain 3.0}. Ich~zdecentralizowana infrastruktura pozwala organizować zbiórki funduszy w~celu realizacji projektów (ang. \textit{crowdfunding}), jak~również wykonywać rozproszone obliczenia w~chmurze\footnote{Użytkownik zleca zdecentralizowanej aplikacji wykonanie interesujących go~obliczeń. Rozproszona sieć tworząca back-end aplikacji rozdziela zadanie pomiędzy węzły, które~obliczają fragmenty wyniku lokalnie. Na~koniec rezultat jest scalany i~przekazywany do~użytkownika, który~płaci za~usługę.} (ang. \textit{cloud computing}). Najnowsza generacja, zgodnie z~konwencją nazwana \textit{Blockchain 4.0} odchodzi od~pierwotnych postulatów. Branża bankowa oraz duże korporacje pracują nad~zaadoptowaniem Blockchain do~własnych potrzeb. W~wyniku analiz ekspertów projektowane są rozproszone rejestry o~charakterze zamkniętym, w~których~użytkownicy pozbawieni są~anonimowości. Celem jest m.in. automatyzacja produkcji, integracja systemów działających niezależnie od~siebie, wydajne planowanie zasobów przedsiębiorstwa czy współdziałanie niezależnych podmiotów bez pośrednictwa trzeciej strony. Najogólniej mówiąc, Blockchain ma pełnić funkcję odpornego na~manipulacje dziennika zdarzeń (ang. \textit{tamper evident log}).


\section{Kryptowaluty}
Pierwsze próby uruchomienia \textit{waluty cyfrowej} (ang. \textit{digital currency}),
czyli środka płatniczego, który~istnieje wyłącznie w~formie elektronicznej i~nie posiada fizycznego odpowiednika, podjęto w~latach 80. XX wieku. Podobnie jak tradycyjne waluty miała być ona wydawana przez bank centralny. Nowatorskie podejście niosło za~sobą automatyzację dokonywanych przelewów oraz~transfery własności ponad granicami państw bez dodatkowych opłat.
U~podstaw działania waluty cyfrowej stał mechanizm \textit{ślepego podpisu} (ang. \textit{blind signature}) stworzony przez Davida Chauma \cite{blind-singatures}. Jest to forma \textit{podpisu cyfrowego}, w~której podpisujący nie zna treści zatwierdzanego dokumentu. Dzięki temu zaufana trzecia strona, taka jak~bank, mogłaby kwitować przelewy bez wglądu do~szczegółów wykonywanych transakcji. Pomysł scentralizowanego zarządzania wywodził się z~konwencjonalnych walut, jednak konieczność istnienia trzeciej, zaufanej strony odpowiadającej za emisję oraz~przekaz pieniądza stanowiła istotną wadę protokołu.

W~1998 roku Wei Dai przedstawił ideę waluty emitowanej
 poprzez rozwiązywanie \textit{puzzli obliczeniowych} (ang. \textit{computational puzzle}), nazwał ją~\textit{b-money} \cite{b-money}. Waluta nie miała centralnego nadzorcy, a~każdy przekaz środków miał być akceptowany, bądź odrzucany na~podstawie konsensusu użytkowników systemu. Nie potrafiono jednak stworzyć rozwiązania, które~gwarantowałoby osiągnięcie konsensusu w~każdym scenariuszu, przez co~b-money stała się koncepcją czysto teoretyczną. 
 
 W~2005 roku Hal Finney wprowadził pojęcie \textit{dowodu wykonania pracy wielokrotnego użytku} (ang. \textit{reusable proof of work}) \cite{reusable-proof-of-work}. Za pomocą nowego mechanizmu rozwiązano problem osiągania konsensusu w~rozproszonej sieci
   i~zaprojektowano system wykorzystujący puzzle obliczeniowe oparte na~funkcjach skrótu (ang. \textit{Hascash puzzles}). Była to~pierwsza implementacja waluty cyfrowej, emitowanej w~sposób rozproszony. Ostatecznie system nie zyskał uznania wśród użytkowników, a~jednym z~powodów było wykorzystanie centralnego serwera do~walidowania i~realizacji transakcji, który~miał być zaufaną trzecią stroną. Podsumowując, jedynie mechanizm emisji został zdecentralizowany.
 
Walutę cyfrową nie posiadającą centralnego zwierzchnika, której~transfer środków jest
  zgodny z~ustalonym protokołem kryptograficznym przyjęło się nazywać \textit{kryptowalutą} (ang. \textit{cryptocurrency}). Tego rodzaju walutę uruchomiono po~raz pierwszy w~2009 roku i~jest ona znana jako \textit{Bitcoin} \cite{bitcoin-invention}.
Jej twórca o~pseudonimie Satoshi Nakamoto wycofał się z~rozwoju systemu i~do dziś pozostaje nieznany. 
Wdrożenie idei waluty cyfrowej do~użytku oraz~zdobycie przez nią dużej popularności okazało się możliwe głównie dzięki dwóm czynnikom: decentralizacji zarządzania oraz~osiąganiu konsensusu przy~pomocy dowodu wykonania pracy. Techniczna realizacja tych koncepcji została oparta na~technologii Blockchain, z~której korzystają wszystkie obecnie funkcjonujące kryptowaluty.


\section{Funkcja skrótu}
W~tym podrozdziale wprowadzimy pojęcie \textit{funkcji skrótu} oraz~\textit{kryptograficznej funkcji skrótu}, będącej jednym z~kluczowych elementów Blockchain. Następnie opiszemy dodatkowe własności, które~powinna posiadać funkcja skrótu, szczególnie istotne dla~bezpiecznego działania technologii Blockchain.

\begin{definition}
Funkcja skrótu (ang. hash function) jest wydajną
 obliczeniowo\footnote{Typowo przyjmuje się, że~operacja obliczenia skrótu dla~ciągu binarnego długości $m$ jest rzędu $\mathcal{O}(m)$.} funkcją
która~przyporządkowuje ciągowi binarnemu skończonej długości skrót, będący ciągiem binarnym
ustalonej długości $n$:
 $$H : \{0, 1\}^* \rightarrow \{0, 1\}^n~.$$
\end{definition}

W~zastosowaniach związanych z~kryptografią i~bezpieczeństwem komputerowym wymagane są~pewne dodatkowe właściwości funkcji haszującej. 

\begin{definition} Kryptograficzną funkcja skrótu
będziemy nazywać funkcję skrótu $H$, która~spełnia następujące własności:
\begin{enumerate}
	\item Nieodwracalność (ang. preimage resistance): dla~zadanego skrótu $y$ znalezienie ciągu binarnego $x$ takiego, że~$H(x) = y$ jest praktycznie nieobliczalne\footnote{Termin \textit{praktycznie nieobliczalne} występujący w~definicjach traktujemy jako odpowiednik popularnego w~literaturze określenia \textit{computationally infeasible}.}.	
	\item Słaba bezkolizyjność (ang. second preimage resistance): dla~zadanego ciągu binarnego $x$ znalezienie $x'$ takiego, że~$x \neq x'$ oraz~$H(x) = H(x')$ jest praktycznie nieobliczalne.
	\item Slina bezkolizyjność (ang. collision resistance): znalezienie pary ciągów binarnych $(x, x')$ takiej, że~$x \neq x'$ oraz~$H(x) = H(x')$ jest praktycznie nieobliczalne.
\end{enumerate}
\end{definition}

Nieodwracalność gwarantuje, że~funkcja ma~charakter jednokierunkowy: obliczenie skrótu dla zadanego wejścia jest łatwe, natomiast wyznaczenie wejścia w~oparciu o~skrót jest w~praktyce niewykonalne. 

Słaba bezkolizyjność zapewnia, że~adwersarz\footnote{\textit{Adwersarzem} nazywamy użytkownika, który~usiłuje znaleźć i~wykorzystać słabe strony przyjętego protokołu.}
 nie jest w~stanie znaleźć innego bloku danych, którego skrót będzie taki sam, jak dla zadanego bloku. Dzięki temu, w~celu zapewnienia integralności danych, wystarczy zapamiętać ich skrót. Przed akceptacją danych wyliczamy dla nich hash i~porównujemy z~zapamiętanym skrótem. Jeżeli nastąpił błąd lub dane zostały zmodyfikowane przez adwersarza, to~wartości hashy będą się różnić.

 Silna bezkolizyjność gwarantuje odporność funkcji na
  \textit{atak urodzinowy} (ang. \textit{birthday attack}) \cite{birthday-attack}.
    Atak urodzinowy jest bezpośrednią konsekwencją \textit{paradoksu urodzinowego} (ang. \textit{birthday problem}) \cite{birthday-problem}.
     Załóżmy, że~moc obrazu rozpatrywanej funkcji skrótu wynosi $2^n$, gdzie $n$ jest pewną stałą.
     Wiemy, że~bloki dla~których zachodzi kolizja muszą istnieć, gdyż dziedzina funkcji jest nieskończona.
     Naiwną metodą gwarantującą znalezienie kolizji jest generowanie różnych wejść i~obliczanie skrótu dla~każdego z~nich. 
     Jeżeli wygenerujemy $2^{n} + 1$ różnych łańcuchów bitów, to~mamy pewność znalezienia kolizji. 
     Atak urodzinowy polega na~liczeniu hashy dla~losowych ciągów bitów, aż do~momentu znalezienia jakiejkolwiek kolizji. Prawdopodobieństwo, że~adwersarz znajdzie kolizję już po~sprawdzeniu $\sqrt{2^{n}}$ wejść wynosi około $\frac{1}{2}$ \cite{birthday-problem}.  
     Dla przykładu rozważmy kryptograficzną funkcję skrótu \textit{SHA-256}, która~zwraca hash o~długości 256 bitów. 
       Przyjmijmy, że~mamy do~dyspozycji maszynę, zoptymalizowaną pod kątem obliczania hashy, która~wyznacza $10^{12}$ skrótów na~sekundę. Do~obliczenia $2^{128}$ skrótów będzie ona potrzebować więcej niż $10^{19}$ lat.
     Zatem powyższe metody znajdowania kolizji są w~praktyce nieskuteczne.
     Nie oznacza to jednak, że~nie istnieje inny, wydajny algorytm znajdowania kolizji dla konkretnej funkcji skrótu. 
     Funkcje skrótu wykorzystywane w~protokołach kryptograficznych są~funkcjami dla~których specjalistom nie udało się opracować efektywnej metody znajdowania kolizji.
     Bywa, że~powszechnie wykorzystywane funkcje skrótu jak np. \textit{MD5} w~związku z~rosnącą mocą obliczeniową maszyn lub~w~wyniku skutecznej metody ataku przestają być uważane za~bezpieczne.  
     Nie istnieje funkcja skrótu dla~której dowiedziono, że~jest silnie bezkolizyjna, a~hipoteza że~dla konkretnych funkcji tak faktycznie jest 
     opiera się jedynie na~wysiłku jaki włożono w~próbę jej obalenia.
     
     Mając do~dyspozycji funkcję $H$, o~której zakładamy, że~jest silnie bezkolizyjna, będziemy przyjmować, że~dla różnych od~siebie danych wejściowych $x \neq x'$, również ich skróty wygenerowane za pomocą tej funkcji będą różne $H(x) \neq H(x')$. 
     Dzięki temu będziemy mogli traktować skrót jako unikalny identyfikator danych (ang. \textit{message digest}) co w~praktyce umożliwi weryfikację poprawności i~integralności danych, bez potrzeby pamiętania ich w~całości.
     

\section{Funkcja skrótu w kontekście technologii Blockchain}
 
 Dla technologii Blockchain istotne jest, aby znalezienie dwóch bloków danych posiadających taki sam skrót było praktycznie niewykonalne, dlatego~kluczową własnością funkcji skrótu będzie silna bezkolizyjność. Ponadto istotne będą również
trzy dodatkowe własności, które omawiamy poniżej: \textit{ukrywanie} (ang. \textit{hiding}), \textit{wiązanie} (ang. \textit{binding}) oraz~\textit{sprzyjanie puzzlom obliczeniowym} (ang. \textit{puzzle friendliness}).


\begin{definition}
Funkcja skrótu $H$ posiada \underline{własność ukrywania} (ang. hiding), jeśli dla~losowo wybranego ciągu $r \in \{0, 1\}^*$ pochodzącego z~rozkładu o~wysokiej entropii\footnote{Tutaj oraz~w~kolejnych definicjach przyjmujemy, że~wybrany rozkład prawdopodobieństwa ma taki sam rząd entropii jak rozkład jednostajny.} oraz~skrótu $y \in \{0, 1\}^n$, takiego, że \mbox{$y = H(r \Vert x)$} znalezienie $x$ jest praktycznie nieobliczalne. Symbol $\Vert$ oznacza konkatenację ciągów binarnych.
\end{definition}
 
  Intuicyjnie, im~większa entropia rozkładu, tym trudniej przewidzieć wynik losowania.
   W~praktyce dane, na~których operujemy nie występują równie często, dzięki czemu adwersarz może zawęzić przeszukiwany zbiór wyników, zyskując przewagę. Wykorzystanie własności ukrywania pomaga zapobiec temu rodzajowi ataków poprzez zwiększenie entropii danych, dla~których obliczany jest hash. Przykładem wykorzystania tej własności jest dodawanie \textit{soli} do~haseł użytkowników. Technika ta~chroni przed atakami tzw. metodą słownikową \cite{dictionary-attack}.

\begin{definition} Funkcja skrótu $H$ jest \underline{wiążąca} (ang. binding), jeśli znalezienie dwóch par $(msg, r)$ i~$(msg', r')$ takich, że $msg \neq msg'$ oraz~$H(msg \Vert r) = H(msg' \Vert r')$ jest praktycznie nieobliczalne.
\end{definition}

Wiązanie wynika wprost z~silnej bezkolizyjności. Dla~dowolnej, kryptograficznej funkcji skrótu $H$, znalezienie par $(msg, r)$ i~$(msg', r')$ takich, że~$H(msg \Vert r) = H(msg' \Vert r')$ jest niemożliwe, ponieważ stanowiłyby one kolizję. Nie oznacza to jednak, że~funkcja wiążąca jest zawsze silnie bezkolizyjna, implikacja pomiędzy własnościami zachodzi tylko w~jedną stronę.
 W~odniesieniu do~Blockchain własności ukrywania oraz~wiązania stosuje się do~tworzenia \textit{zobowiązań bitowych}. Zobowiązanie polega na~zadeklarowaniu posiadania pewnej wartości, którą~w~wybranym momencie ujawnia się innym użytkownikom. Od~chwili złożenia deklaracji, wartość ta nie może ulec zmianie.

\begin{definition} Zobowiązanie bitowe (ang. \textit{commitment scheme}) jest parą funkcji $(C, V)$ takich, że: \label{commitment-scheme}
\begin{enumerate}
	\item $C : \{0, 1\}^* \times \{0, 1\}^* \rightarrow \{0, 1\}^n$ jest funkcją zobowiązania, która~dla wybranego bloku danych block oraz~losowo wybranej wartości r zwraca zobowiązanie $com = C(block, r)$.
	\item $V : \{0, 1\}^n \times \{0, 1\}^* \times \{0, 1\}^* \rightarrow \{0, 1\}$ jest funkcją weryfikacji, która~określa czy podany blok danych $block$ oraz~wartość $r$ odpowiadają zobowiązaniu $com$:
		\[
		V(com, block, r) =
		\begin{cases}
		1, & \text{gdy $com = C(block, r)$}, \\
		0, & \text{w.p.p.}.
		\end{cases}
		\]
	\item Funkcja $C$ posiada własność ukrywania i~jest wiążąca.
\end{enumerate}
\end{definition}
 
Do~stworzenia zobowiązania bitowego niezbędna jest losowa wartość \textit{r}. Każde zobowiązanie wymaga nowego wylosowania \textit{r} aby zachować bezpieczeństwo schematu. Mając blok danych \textit{block} i~\textit{r} obliczamy, a~następnie publikujemy zobowiązanie \textit{com}. Dzięki ukrywaniu nikt nie jest w~stanie wyznaczyć wartości \textit{block} korzystając z~\textit{com}. Dodatkowo własność wiązania uniemożliwia podmianę zadeklarowanego bloku danych.



Ostatnia właściwość funkcji skrótu, którą omawiamy poniżej jest ściśle związana z~\textit{dowodem wykonania pracy} (ang. \textit{proof of work}).
Dowód wykonania pracy to~idea na~której często opiera się osiąganie konsensusu w~systemach rozproszonych (np. dla większości kryptowalut).
Użytkownicy systemu wypracowują konsensus na~podstawie emitowanych w~sieci bloków danych, co pozwala jednoznacznie określić aktualny stan łańcucha bloków.

\begin{definition} \label{puzzle-friendliness}
Funkcja skrótu $H$ sprzyja \underline{puzzlom obliczeniowym}  (ang. puzzle friendliness), jeśli dla~ustalonego $y \in \{0, 1\}^n$ oraz~losowo wybranego ciągu $r \in \{0, 1\}^*$ pochodzącego z~rozkładu o~wysokiej entropii znalezienie $x \in \{0, 1\}^*$ takiego, że~$H(r \Vert x) = y$ nie jest wykonalne w~liczbie prób istotnie mniejszej niż~$2^n$.
\end{definition}

Definicję \ref{puzzle-friendliness} można wykorzystać do~tworzenia \textit{puzzli obliczeniowych}, czyli problemów, które wymagają dużych nakładów obliczeniowych, np. przeszukania dużych zbiorów w~celu znalezienia rozwiązania. W~szczególności nie powinien istnieć  sposób, który~pozwoli adwersarzowi na~efektywne ograniczenie przeszukiwanego zbioru potencjalnych rozwiązań. 
 W praktyce często zamiast wartości $y$ ustala się zbiór $Y$ tzw. rozwiązań dopuszczalnych. Oczywiście, im większa moc zbioru $Y$, tym puzzel obliczeniowy jest łatwiejszy do rozwiązania.

\begin{definition} Puzzel obliczeniowy (ang. computational puzzle)
 jest problemem, dla~którego dana jest funkcja skrótu $H$, ciąg $r \in \{0, 1\}^*$ wylosowany z~rozkładu o~wysokiej entropii oraz~zbiór rozwiązań dopuszczalnych $Y$. Rozwiązaniem problemu jest wartość $x$ taka, że~$H(r \Vert x) \in Y$.
\end{definition}

Wymaganie aby~$r$ pochodziło z~rozkładu o~wysokiej entropii ma zapobiec sytuacji, w~której adwersarz obliczyłby wiele rozwiązań dla~konkretnego, często pojawiającego się $r$, zyskując tym~samym przewagę. Jeżeli funkcja skrótu sprzyja puzzlom obliczeniowym, to~dla~zbudowanego na~niej problemu obliczeniowego nie istnieje  strategia istotnie lepsza, niż losowanie $x$ i~sprawdzanie czy wynik obliczenia jest elementem zbioru $Y$. Idea ta odgrywa szczególnie istotną rolę m.in. w~\textit{wydobywaniu} (ang. \textit{mining}) Bitcoina, do~czego wrócimy w rozdziale \ref{bitcoin}.\\


\section{Transformacja Merkle-Damg\aa rd}
Omówiliśmy już wszystkie własności funkcji skrótu potrzebne do dalszych rozważań. 
Od~funkcji skrótu wymagamy aby~operowała na~danych o~dowolnym zakresie.
Pokażemy teraz, że w praktyce wystarczy skonstruować pewną  funkcję $K : \{0, 1\}^m \rightarrow \{0, 1\}^n$, gdzie $m > n$, która~przyjmuje dane ustalonego rozmiaru i~wykorzystać tzw. \textit{transformację Merkle-Damg\aa rd} \cite{merkle-damgard-construction}, aby otrzymać funkcję działającą na~wejściu o~dowolnej długości. Kryptograficzna funkcja skrótu $K$ działająca na~danych stałego rozmiaru, która~leży u~podstaw transformacji, nosi nazwę \textit{funkcji kompresującej} (ang. \textit{compression function}).

Transformacja Merkle-Damg\aa rd działa w~następujący sposób. Wejście kryptograficznej funkcji skrótu, którego~rozmiar nie jest określony zostaje podzielone na~zbiór bloków: $$B = \{b_i : \left(\forall i \in \ceil*{\frac{x}{m-n}}\right)\left(\vert b_i\vert = m - n\right)\}~,$$ gdzie $x$ jest ilością bitów potrzebnych do~zapisu wejścia. W~każdym kroku blok danych i~wynik poprzedniej iteracji jest podawany do~funkcji kompresującej $K$. Zwróćmy uwagę, że~całkowity rozmiar wejścia do funkcji kompresującej wyniesie $(m - n) + n = m$, zgodnie z~definicją funkcji $K$. Dla pierwszego bloku zamiast poprzedniego wyniku dodajemy \textit{wektor inicjujący} (ang. \textit{initialization vector, IV}). $IV$ jest zaszyte w~implementacji funkcji $K$ i~ma jawną oraz~stałą wartość. Wynik obliczeń dla~ostatniego bloku jest końcowym skrótem, który~otrzymamy. Tym samym, transformacja przekształca dane
dowolnej długości w skrót długości $n$. Zostało dowiedzione, że~jeżeli funkcja kompresująca jest silnie bezkolizyjna, to~również funkcja uzyskana z~transformacji Merkle-Damg\aa rd jest silnie bezkolizyjna.

\begin{lemma}
Transformacja Merkle-Damg\aa rd  przekształca silnie bezkolizyjną funkcję kompresującą $K : \{0, 1\}^m \rightarrow \{0, 1\}^n$, gdzie $m > n$, w silnie bezkolizyjną funkcję $H : \{0, 1\}^* \rightarrow \{0, 1\}^n$.
\end{lemma} 

\begin{figure}[htbp]
	\centering
	\includegraphics[scale=0.75]{diagrams/merkle-damgard-construction.pdf}
	\caption{Schemat działania transformacji Merkle-Damg\aa rd.}
	\label{merkle-damgard-construction}
\end{figure}





Przykładem kryptograficznej funkcji skrótu, otrzymanej w~wyniku zastosowania transformacji Merkle-Damg\aa rd jest SHA-256. Posłużyła ona do~stworzenia pierwszego Blockchain, które~powstało wraz~z~Bitcoinem. Przyjmuje się, że SHA-256 posiada wszystkie przedstawione powyżej właściwości. Długość produkowanego skrótu wynosi $256$ bitów.
SHA-256 używa funkcji kompresującej, dla~której~$m = 768$, natomiast $n = 256$. Wynika stąd, że~wektor IV również jest zapisywany za~pomocą $256$ bitów. W~przypadku gdy~rozmiar danych wejściowych nie jest wielokrotnością $512$ bitów, ostatni blok zostaje uzupełniony. Najpierw dodawany jest bit $1$, a~po nim doklejane są~zera do~momentu, w~którym~blok osiąga długość $512$ bitów.







\section{Podpisy cyfrowe}
Drugim istotnym narzędziem kryptograficznym, poza funkcją skrótu, wykorzystywanym w~technologi Blockchain jest~\textit{podpis cyfrowy}.   Główne zastosowanie podpisów cyfrowych jest analogiczne do~podpisów składanych ręcznie: wymagamy aby~podpis identyfikował jednoznacznie jego właściciela. Tylko posiadacz podpisu powinien mieć możliwość złożenia go, ale~istnieje metoda pozwalająca na~weryfikację autentyczności podpisu przez każdego użytkownika systemu. Ponadto podpis cyfrowy ma być związany z~konkretnym dokumentem i~nie powinna istnieć żadna możliwość jego skopiowania lub przenoszenia między dokumentami. Poniżej pokrótce omówimy ideę konstruowania podpisów cyfrowych. 

W~praktyce podpisy cyfrowe są związane z~tzw. \textit{kryptografią klucza publicznego}, która~opiera się na~parze kluczy: publicznym oraz~prywatnym. Klucz jest ciągiem binarnym 
o~ustalonej długości\footnote{W systemie Bitcoin rozmiar klucza prywatnego oraz~długość skrótu generowanego przez funkcję haszującą są~sobie równe i~wynoszą 256 bitów. Klucz publiczny ma 520 bitów.}. Klucz prywatny $sk$ jest trzymany w~sekrecie, wykorzystuje się go~do~składania podpisów. Klucz publiczny $pk$ jest powszechnie udostępniany i~służy do~weryfikacji złożonego podpisu.

\begin{definition} Podpis cyfrowy (ang. digital signature) jest trójką algorytmów $(gen, sign, verify)$ takich, że:
\begin{enumerate}
	\item $gen$ przyjmuje na~wejściu parametr $t$ determinujący rozmiar kluczy i~zwraca parę $(sk, pk)$. 
	\item $sign$ dla~bloku danych $block$ i~klucza prywatnego $sk$ tworzy podpis $sig$.
	\item $verify$ przyjmuje blok danych $block$, klucz publiczny $pk$ oraz~wygenerowany podpis $sig$. Wynikiem algorytmu jest bit $1$ wtedy gdy~$sig$ jest poprawnym podpisem dla~danych z~wejścia, natomiast w~przeciwnym przypadku zwracany jest bit $0$.
\end{enumerate}

Dodatkowo podpis cyfrowy posiada następujące cechy:

\begin{enumerate}
	\item Przy założeniu, że~$sig$ jest poprawnym podpisem utworzonym dla~klucza $sk$ i~bloku danych $block$, poniższe równanie jest zawsze prawdziwe:
	\begin{center}
	$verify(block, pk, sign(block, sk)) = 1.$
\end{center}
	\item Podrobienie podpisu dla~dowolnego bloku danych jest praktycznie nieobliczalne.
\end{enumerate}
\end{definition}
 
 Działanie schematu może być opisane za~pomocą gry pomiędzy adwersarzem starającym się podrobić podpis i~właścicielem podpisu. W~pierwszym kroku gry uruchamiamy algorytm $(sk, pk) \coloneqq gen(t)$.
  Właściciel podpisu otrzymuje parę $(sk, pk)$, natomiast adwersarz ma~dostęp jedynie do~klucza publicznego $pk$. Zadaniem adwersarza jest podszycie się pod~właściciela i~wygenerowanie fałszywego podpisu cyfrowego $sigAdv$ dla~dowolnego, wybranego przez siebie bloku danych $B$. Innymi słowy $sigAdv$ musi spełniać równanie $verify(B, pk, sigAdv) = 1$.
  Gra ma~odzwierciedlać rzeczywisty scenariusz, w~którym adwersarz może przyglądać się istniejącym podpisom ofiary, a~niekiedy również nakłonić ją do podpisania specjalnie spreparowanego dokumentu. Pozwalamy zatem atakującemu przyjrzeć się poprawnym podpisom dla~przygotowanych przez niego dokumentów: dokument wysyłany jest do~posiadacza klucza prywatnego, który~sygnuje go i~odsyła z~powrotem\footnote{Zakładamy, że czynność ta może zostać powtórzona wielomianową liczbę razy względem rozmiaru klucza $n$.}.
  Ostatecznie adwersarz wybiera  blok danych $B$ oraz tworzy podrobiony podpis $sigAdv$, który~powinien przejść weryfikację z~wykorzystaniem klucza publicznego $pk$. Oczywiście blok $B$ musi różnić się od~dotychczas podpisanych bloków, inaczej zadanie byłoby trywialne.
   Właściciel podpisu wykonuje algorytm $x \coloneqq verify(B, pk, sigAdv)$, jeśli zwrócone zostanie $x = 1$, to~atakujący sfałszował podpis, tym~samym wygrywając grę. W~przeciwnym wypadku atak zakończył się fiaskiem. 

\begin{figure}[htbp]
	\centering
	\includegraphics[scale=0.55]{diagrams/unforgeability-game.pdf}
	\caption{Bezpieczeństwo podpisu cyfrowego jako gra pomiędzy właścicielem i~adwersarzem.}
	\label{unforgeability-game}
\end{figure}

Do~poprawnego działania schematu niezbędne jest dobre źródło losowości, na~którym bazuje algorytm $gen$. Gdy źródło jest przewidywalne, to~schemat odporny na~ataki przeprowadzane podczas omówionej gry i~tak może się okazać podatny na~złamanie. Istotnym aspektem jest także długość sygnowanego dokumentu. W~praktyce algorytm $sign$
działa na~danych ograniczonego rozmiaru. Rozwiązaniem może być podpisywanie wyliczonego skrótu, zamiast treści dokumentu.

Dwoma popularnymi standardami podpisu cyfrowego są RSA oraz DSA \cite{digital-signatures}.
DSA może być realizowany w~oparciu o~logarytmy dyskretne lub krzywe eliptyczne (\textit{Elliptic Curve Digital Signature Algorithm} - ECDSA) \cite{ecdsa}.
Właśnie ten ostatni sposób jest powszechnie wykorzystywany w~implementacjach technologii Blockchain.


\section{Podpisy cyfrowe w kontekście technologii Blockchain}
Transakcje zapisane w~Blockchain mają trwały charakter i~są odporne na~manipulacje, stąd~transfer kryptowaluty pomiędzy użytkownikami jest nieodwracalny. Dotyczy to~również sytuacji w~której~środki zostały przekazane omyłkowo błędnemu adresatowi, bądź doszło do~kradzieży. Zrealizowane transakcje są jawne dla uczestników systemu, którzy mają wgląd do~pełnej historii Blockchain. Co~więcej kryptowaluta ma charakter wyłącznie cyfrowy przez co przypisanie środków do~konkretnego użytkownika i~uniemożliwienie wydania ich przez osoby nieupoważnione jest utrudnione. Problem przynależności środków do~jednego, konkretnego użytkownika rozwiązano wprowadzając do~Blockchain podpisy cyfrowe. Schemat podpisów cyfrowych nie chroni nabywców kryptowaluty przed wykonaniem błędnej transakcji, ale~wyklucza sytuację w~której niepowołana osoba bezpowrotnie wydaje środki należące do~innego użytkownika. Jedynie posiadacz klucza prywatnego $sk$ jest w~stanie wydać przynależne mu środki.
 
Ponadto w~systemach opartych na~Blockchain klucz publiczny $pk$ można rozumieć jako tożsamość użytkownika. Jeżeli transakcja posiada podpis, którego~prawdziwość potwierdza weryfikacja kluczem $pk$, to~znaczy, że~użytkownik skojarzony z~$pk$ ją~aprobuje. Może to~zrobić jedynie on, ponieważ nikt inny nie ma~dostępu do~klucza prywatnego $sk$, niezbędnego do wykonania podpisu.
Zauważmy, że w takim ujęciu nie ma żadnych ograniczeń w~tworzeniu nowych tożsamości. 
 Każdy użytkownik może wygenerować dowolnie wiele par kluczy, zakładając tym samym nowe konta, które~nie są ze~sobą skojarzone, pomimo że~stoi za~nimi jeden właściciel. 
W~praktyce, im~więcej transakcji jest zrealizowanych przy użyciu danej pary kluczy tym~łatwiej odkryć faktyczną tożsamość ich właściciela. 

Często za~tożsamość użytkownika przyjmujemy hash klucza publicznego $pk$, a~nie sam klucz. 
Ustalenie tożsamości użytkownika, który~podpisał  transakcję składa się wtedy z~dwóch kroków. W~pierwszym sprawdzamy czy skrót $pk$ i~hash, którym~legitymuje się użytkownik są~równe. Następnie weryfikujemy transakcję bezpośrednio korzystając z~$pk$.

Podkreślmy, że przedstawiony sposób kreowania kont jest w~pełni zdecentralizowany: nie istnieje wspólny zarządca rejestrujący nowych użytkowników.
Nie występuje również powszechny spis uczestników systemu i~nikogo nie trzeba informować o wygenerowaniu nowej pary kluczy. W~taki sposób działa m.in. system Bitcoin, gdzie~skrót klucza publicznego nazywany jest \textit{adresem}.


