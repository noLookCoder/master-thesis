\chapter{Symulacja ataku double spend} \label{simulation}
\thispagestyle{chapterBeginStyle}
Ten~rozdział został poświęcony symulacji ataku double spend w  kontekście istnienia kopalni, które~coraz częściej formują użytkownicy popularnych systemów kryptowalut, m.in. Bitcoin czy~Ethereum. W rozdziale \ref{bitcoin} zaznaczyliśmy, że~wiele z~istniejących kopalni posiada moc obliczeniową stanowiącą istotną część mocy jaką dysponuje cała sieć.
Skupimy się na potencjalnie negatywnych efektach tego zjawiska i~zagrożeniach, które~ono stwarza. 
Zauważmy ponadto, że analiza problemu double spend przedstawiona w~rozdziale \ref{double-spend-analysis} nie uwzględnia czasu potrzebnego na~propagację bloku w~sieci. Uwzględnienie opóźnień w formalnej analizie wydaje się trudne, 
dlatego zdecydowaliśmy się zaimplementować symulator, który~ma za~zadanie możliwe wiernie odtworzyć rzeczywiste zachowanie systemu dla różnych topologi sieci.
 W rozdziale przedstawiamy wyniki eksperymentów przeprowadzonych za pomocą symulatora i~płynące z~nich wnioski.


\section{Wektor ataku} \label{selfish-pool-attack}
Atak double spend został opisany w~podrozdziale \ref{double-spend-overview}. Teoretyczną analizę problemu przedstawiliśmy w~rozdziale \ref{double-spend-analysis}.
Niekiedy przyjmuje się, że~atak kończy się sukcesem gdy~adwersarz zbuduje ciąg bloków, którego~długość jest równa aktualnie najdłuższej gałęzi. W~praktyce nie oznacza to~jednak zwycięstwa adwersarza, ponieważ uczciwe węzły mogą kontynuować pracę nad~rozszerzeniem najdłuższej gałęzi do której mieli początkowo dostęp\footnote{W takim scenariuszu istotną rolę zaczna grać czas propagacji bloków w~sieci.}. Z~tego powodu przyjmujemy, że~double spend atak powiódł się w~momencie, gdy~adwersarz znalazł ciąg bloków dłuższy od~aktualnej gałęzi łańcucha.

Zjawisko wspólnej pracy użytkowników nad~wydobyciem bloków występuje w~większości liczących się systemów kryptowalut. Uformowane w~ten sposób kopalnie wymagają utworzenia centralnego serwera. Serwer koordynuje pracę kopalni i~rozsyła do~górników bloki, dla~których szukają oni dowodu wykonania pracy. Proces ten~wprowadza pewien poziom centralizacji sieci. Dzięki przejęciu kontroli nad~centralnym serwerem, adwersarz mógłby nadać kierunek pracy górnikom zrzeszonym w~ramach kopalni, w~taki sposób aby poświęcali swoją moc obliczeniową na~rozszerzanie alternatywnej gałęzi łańcucha. Innymi słowy łączna moc obliczeniowa kopalni przypadłaby adwersarzowi, który~mógłby wykorzystać ją~do~przeprowadzenia ataku double spend.

Aktualny rozkład mocy obliczeniowej kopalni działających w~Bitcoinie został umieszczony na~diagramie \ref{mining-pools-distribution}. Widzimy, że~kopalnia o~nazwie \texttt{BTC.com} posiada blisko jedną trzecią łącznej mocy obliczeniowej sieci. Zatem omówiony wektor ataku ma~realną szansę powodzenia, a~moc którą~dysponowałby adwersarz wystawia na~próbę~bezpieczeństwo Bitcoina oraz~innych kryptowalut. Warto dodać, że~przyjęte modele Blockchain są różne w różnych systemach kryptowalut. Istotnym parametrem modelu, który~wpływa na~powodzenie ataku double spend jest czas potrzebny na~transmisję bloków pomiędzy węzłami sieci. Inne właściwości modelu odgrywające kluczową rolę to: liczba bloków, którą~należy odczekać by~uznać transakcję za~zrealizowaną oraz~topologia sieci.

Rozważamy również wpływ strategii \textit{samolubnego wydobywania bloków} (ang. \textit{selfish mining}) \cite{selfish-mining} na skuteczność ataku double spend. 
Polega ona na~rozszerzaniu aktualnej gałęzi łańcucha bez natychmiastowego propagowania wydobywanych bloków do~sąsiadujących węzłów.
Dzięki temu adwersarz mógłby wydobyć kilka bloków w~przód, zanim uczciwe węzły przystąpią do~pracy. Adwersarz kontynuuje rozszerzanie alternatywnej gałęzi w~tajemnicy i~czeka na~moment, w~którym interesująca go~transakcja uzyska wystarczającą liczbę potwierdzeń by~uznać ją~za~zrealizowaną. Jeżeli od~tej chwili alternatywna gałąź, nad którą~pracuje adwersarz kiedykolwiek stanie się dłuższa od~aktualnej gałęzi w~łańcuchu, to~jej natychmiastowa propagacja kończy się pomyślnym przeprowadzeniem double spend. Opisana strategia jest możliwa do~wykonania w~praktyce i~daje istotną przewagę adwersarzowi w~stosunku do~podejścia, w~którym adwersarz czeka na~zrealizowanie interesującej go~transakcji by~rozpocząć atak.

Warto zwrócić uwagę, że~czas trwania ataku działa na~niekorzyść adwersarza jeśli dysponuje on mniejszą mocą obliczeniową niż łącznie uczciwe węzły. W~szczególności w~rozdziale \ref{double-spend-analysis} pokazaliśmy, że~prawdopodobieństwo powodzenia ataku spada wykładniczo wraz~z~ilością  bloków wydobytych przez uczciwe węzły.
Nie rozważamy przypadku, w~którym~adwersarz posiada większość łącznej mocy obliczeniowej sieci, ponieważ mógłby on~wtedy z~powodzeniem przeprowadzić majority attack (zobacz podrozdział \ref{majority-attack-overview}).
Zauważmy, że jeśli adwersarz nigdy nie dogoni łańcucha budowanego przez  uczciwe węzły, 
wówczas samolubne wydobywanie bloków kończy się nie tylko niepowodzeniem ataku, ale~również brakiem nagrody za wydobyte bloki, ponieważ~nie zostały one włączone do~aktualnej gałęzi łańcucha.


\section{Symulator} \label{simulator}
Symulator atatku double spend został zaimplementowany w~języku \texttt{Python} \cite{modern-python-cookbook}.
Wykresy przedstawiające wyniki eksperymentów w~podrozdziale \ref{experiment-results} zostały wygenerowane przy użyciu biblioteki \texttt{matplotlib}. Symulację rozkładu wykładniczego oraz~obliczenia matematyczne przeprowadzono korzystając z~biblioteki \texttt{numpy}, która~dedykowana jest obliczeniom naukowym. 
Rozważane modele sieci wizualizowaliśmy za~pomocą zestawu funkcji pochodzących z~pakietu \texttt{networkx}. Do~uruchomienia symulatora niezbędna jest instalacja języka \texttt{Python} w~wersji trzeciej oraz~wymienionych wcześniej bibliotek: \texttt{matplotlib}, \texttt{numpy} i~\texttt{networkx}. Większość instalatorów języka \texttt{Python} domyślnie dostarcza te~biblioteki.

Zadaniem symulatora było wierne odzwierciedlenie mechanizmu działania Blockchain. Program pozwala zdefiniować liczbę uczciwych górników oraz~moc obliczeniową każdego z~nich, a~także moc adwersarza. Ponadto~korzystając z~pliku \texttt{input.py} można ręcznie ustalić topologię sieci, bądź użyć generatora topologii, który~został dołączony do~projektu. Dla~podanej liczby węzłów sieci, generator pozwala na~utworzenie topologii: grafu pełnego, pierścienia, linii oraz~Erd\H os - R\'enyi z~zadanym prawdopodobieństwem $p$. Symulator umożliwia również określenie czasu transmisji bloku pomiędzy węzłami sieci $t$. Dodatkowo sparametryzowane zostało maksymalne opóźnienie w~transmisji bloku $d \geq 0$. Jeżeli $d > 0$, to~całkowity czas transmisji wynosi $t + x$, gdzie~$x$ pochodzi z~rozkładu jednostajnego ciągłego na~przedziale $[0, d]$. Konfiguracji podlegają także początkowa przewaga adwersarza w~blokach $a$, która~może wynikać z~zastosowania strategii samolubnego wydobywania bloków. Kolejnym parametrem, jest średni czas wydobycia bloku przez~całą sieć $\mu$. Następny parametr to~liczba potwierdzeń $c$, która~wystarcza użytkownikowi systemu, by~uznać transakcję za~zrealizowaną. Należy zaznaczyć, że~parametry $a$ oraz~$c$ modelują dwie różne cechy systemu Blockchain i~jeden nie może zostać zastąpiony drugim. Wartość $a$ definuje początkową stratę (kiedy~$a < 0$), bądź przewagę (gdy~$a > 0$) adwersarza względem uczciwych węzłów w~znalezionych blokach. Stała $c$ określa natomiast minimalną liczbę bloków, którą~musi wydobyć adwersarz, a~także moment po~którym gałąź adwersarza może zostać opublikowana, aby~double spend się powiódł. Zbyt wczesna propagacja bloków atakującego spowoduje, że~uczciwe węzły zaakceptują nową gałąź, ale~nie dojdzie do~podwójnego wydania środków, ponieważ~pierwotna transakcja nie zostanie nigdy uznana za~zrealizowaną. Dzięki opisanej parametryzacji symulator pozwala przetestować wiele różnych modeli kryptowalut opartych na technologii Blockchain.

Zgodnie z~analizą zaprezentowaną w~rozdziale \ref{double-spend-analysis} proces wydobywania bloków można modelować jako proces Poissona, a~czas pomiędzy znalezieniem kolejnych bloków dla~całej sieci definiuje zmienna losowa $T$ pochodząca z~rozkładu wykładniczego z~parametrem $\alpha = 1/\mu$. Jeśli przyjmiemy~$\mu = 10$ minut, to~wartość zmiennej losowej $T$ można interpretować jako czas wydobycia bloku dla~systemu Bitcoin.

Symulator działa w~dwóch trybach. Pierwszy tryb pozwala uruchomić pojedynczą symulację ataku double spend dla~konfiguracji zawartej w~pliku \texttt{input.py}. Drugi tryb uruchamia serię  eksperymentów, których wyniki prezentujemy w~podrozdziale \ref{experiment-results}.

{\small
\begin{pseudokod}[htbp]
\SetArgSty{normalfont}
\SetKwFunction{simulation}{simulation}
\SetKwFunction{isDoubleSpendNotSucceeded}{isDoubleSpendNotSucceeded}
\SetKwFunction{isDoubleSpendProbable}{isDoubleSpendProbable}
\SetKwProg{Function}{Function}{:}{}
\SetKwFunction{extendBranch}{extendBranch}
\SetKwProg{Procedure}{Procedure}{:}{}

\KwIn{Adversary initial advantage in blocks $a$\\
Minimal number of valid transaction confirmations $c$\\
Block transmission time $t$\\
Maximal network latency $d$\\
Average time to mine block $\mu$\\
Honest branch advantage threshold $w$}
\KwOut{Attack success $true$ or $false$}

\Function{\simulation($a$, $c$, $t$, $d$, $\mu$, $w$)} {
$honestBranchLength \leftarrow 0$\;
$maliciousBranchLength \leftarrow 0$\;
$miners \leftarrow initMinersWithAdversary(a)$\;
$eventQueue \leftarrow initEventQueue()$\;
\ForEach{$m \in miners$} {
	$eventQueue.push(m.mineNewBlock(\mu))$\;
}
\While{isDoubleSpendNotSucceeded($honestBranchLength$, $maliciousBranchLength$, $c$) \textbf{and} isDoubleSpendProbable($honestBranchLength$, $maliciousBranchLength$, $w$)} {
	$event \leftarrow eventQueue.pop()$\;
	\If{$event$ indicate new block} {
		$owner \leftarrow event.getOwner()$\;
		\If{$event$ is current for $owner$} {
			$extendBranch(owner, event, honestBranchLength, maliciousBranchLength)$\;
			$owner.propagateCurrentBlock(event, d, t)$\;
			$eventQueue.push(owner.mineNewBlock(\mu))$\;
		}
	}
	\Else{
		$recipient \leftarrow event.getRecipient()$\;
		\If{$event$ is current for $recipient$} {
			$recipient.propagateCurrentBlock(event, d, t)$\;
			$eventQueue.push(recipient.mineNewBlock(\mu))$\;
		}
	}
}
\Return{$maliciousBranchLength > honestBranchLength$}\;
}

\Procedure{\extendBranch($owner$, $event$, $honestBranchLength$, $maliciousBranchLength$)} {
	\If{$owner$ is adversary} {
		$maliciousBranchLength \leftarrow maliciousBranchLength + 1$\;
	}
	
	\ElseIf{$event.getBlockNumber() > honestBranchLength$} {
		$honestBranchLength \leftarrow honestBranchLength + 1$\;
	}
}

\Function{\isDoubleSpendNotSucceeded($honestBranchLength$, $maliciousBranchLength$, $c$)}{
	\Return{$maliciousBranchLength \leq honestBranchLength$ \textbf{or} $maliciousBranchLength \leq c$}\;
}

\Function{\isDoubleSpendProbable($honestBranchLength$, $maliciousBranchLength$, $w$)}{
	\Return{$honestBranchLength - maliciousBranchLength < w$}\;
}
\caption{Algorytm symulatora ataku double spend} \label{simulator-algorithm}
\end{pseudokod} 
}

Na~listingu \ref{simulator-algorithm} przedstawiony został algorytm symulatora. Wykonywanie symulacji przerywane jest w~dwóch przypadkach. W~pierwszym przypadku adwersarzowi udało się przegonić uczciwe węzły. Sytuacja ta~musi zajść po~wystąpieniu bloku o~numerze większym od~$c$  (w~Bitcoinie $c = 6$). Inaczej pierwotna transakcja nie zostałaby uznana za~zrealizowaną i atak by się nie powiódł.
 W~drugim przypadku uczciwe węzły zyskały na~tyle dużą przewagę, że~adwersarz ma~znikome szanse przeprowadzenia skutecznego ataku.
 Zakładamy, że~adwersarz zaprzestaje ataku gdy~uczciwi górnicy zyskają nad~nim przewagę $w$ bloków, gdzie$w$ jest parametrem wejściowym programu. 
 
 
 Symulator korzysta z~koncepcji kolejki zdarzeń, na~której przechowywane są~wydarzenia w~porządku zgodnym z~momentem ich wystąpienia w~sieci. Rozróżniamy dwa rodzaje zdarzeń: wydobycie nowego bloku oraz~przekazanie informacji o~nowym bloku pomiędzy węzłami. W~przypadku adwersarza, którego~utożsamiamy z~serwerem centralnym dowodzącym kopalnią, opóźnienia wynikające z~czasu potrzebnego na~transmisję bloków zostały pominięte. Możemy dokonać takiego uproszczenia, gdyż~kopalnia charakteryzuje się topologią gwiazdy, stąd~czas transmisji bloku wewnątrz kopalni jest pomijalnie mały.
W~każdej iteracji z~kolejki zdarzeń ściągane jest najbliższe czasowo zdarzenie. Niekiedy~górnik otrzymuje informację o~nowym bloku zanim zdąży wydobyć własny blok. W~tej sytuacji rozpoczyna on~szukanie dowodu wykonania pracy od~nowa, a~zdarzenie związane z~wydobyciem przez niego bloku na~bazie nieaktualnego już bloku jest ignorowane.

\section{Wyniki eksperymentów} \label{experiment-results}
Przy użyciu symulatora opisanego w~podrozdziale \ref{simulator} przeprowadzone zostało 9 różnych eksperymentów. Dla uproszczenia średni czas wydobycia bloku w~sieci został znormalizowany $\mu = 1$ i rozważone zostały, następujące~czasy potrzebne na~transmisję bloku pomiędzy węzłami: $t_1 = \frac{1}{200}$, $t_2 = \frac{1}{20}$ i~$t_3 = \frac{1}{2}$. Liczba węzłów sieci $n = 30$ została wybrana ze~względu na~kosztowność czasową symulacji, która~wynika m.in. z~dużej liczby przeprowadzanych eksperymentów. Wydaje się, że~większy rozmiar sieci pozwoli jedynie rozważać mniejsze, bardziej realistyczne czasy transmisji.
 Dla liczby potwierdzeń, po~której~możemy uznać transakcję za~zrealizowaną, przyjmujemy $c = 6$, tak jak jest w systemie Bitcoin. Przyjęte w~eksperymentach moce adwersarza wynoszą: 15\%, 30\% oraz~50\% całkowitej mocy obliczeniowej sieci. W~badaniach przetestowane zostały topologie: liniowa, graf pełny i~model Erd\H os - R\'enyi \cite{erdos-renyi}. Dla ostatniej z~nich prawdopodobieństwo wystąpienia krawędzi pomiędzy $n=30$ wierzchołkami grafu równa się $p = 0.125$ i zostało dobrane tak, aby~generowane grafy były stosunkowo rzadkie i~jednocześnie spójne z~dużym prawdopodobieństwem.
 
Jeżeli parametry eksperymentu różnią się od~wyżej przedstawionych, wtedy~informacja o~wartości konkretnego parametru została zawarta w~jego opisie. Rezultaty każdego eksperymentu przedstawiono w~formie wykresu oraz~tabeli z~dokładnymi wynikami. Każdy punkt wykresu został wygenerowany na~podstawie 3~000 powtórzeń symulacji. W~przypadku histogramu \ref{erdos-renyi-winning-blocks-histogram} symulację kontynuowano do~momentu, w~którym adwersarz odniósł 1000 zwycięstw. 


\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{plots/erdos-renyi-winning-blocks-histogram}
	\caption{Relacja pomiędzy numerem bloku,
	w którym następuje zwycięstwo, a~liczbą zwycięstw adwersarza. Symulacja została przerwana po~odniesieniu 1000 zwycięstw przez adwersarza.}
	\label{erdos-renyi-winning-blocks-histogram}
\end{figure}


W~pierwszym eksperymencie wszystkie parametry zostały ustawione na~domyślne wartości określone powyżej. Celem eksperymentu było zbadanie relacji pomiędzy numerem bloku,
w~którym~następuje zwycięstwo, a~liczbą zwycięstw adwersarza. Wyniki zaprezentowano w~formie histogramu.

Wyniki drugiego eksperymentu przedstawiono
w tabeli \ref{adversary-advantage-plot-results} i na rysunku \ref{erdos-renyi-adversary-advantage-plot}.
Wyjaśnia on wpływ strategii samolubnego wydobywania bloków na~prawdopodobieństwo sukcesu ataku double spend. Ponownie wszystkie parametry symulacji przyjęły wartość domyślną. Rozważania mają sens jedynie dla~całkowitych wartości bloków, stąd rezultaty umieszczono na~wykresie punktowym.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{plots/erdos-renyi-adversary-advantage-plot}
	\caption{Powodzenie ataku double spend w~zależności od początkowej przewagi adwersarza w~wydobytych blokach względem uczciwych węzłów sieci. Ujemne wartości na~osi $X$ odpowiadają sytuacji, w~której adwersarz rozpoczął atak w~momencie gdy~uczciwe węzły zdążyły wydobyć już określoną liczbę bloków.}
	\label{erdos-renyi-adversary-advantage-plot}
\end{figure}

\begin{table}[htbp]
	\centering
	\small
	\begin{tabular}{| C{5cm} | c |} 
 		\hline
 		\textbf{Długość gałęzi adwersarza względem uczciwych węzłów} & \textbf{Liczba udanych ataków [\%]} \\
		\hline
		-4 & 0.43 \\
		\hline
		-3 & 1.03 \\
		\hline
		-2 & 2.26 \\
		\hline
		-1 & 4.23 \\
		\hline
		0 & 8.47 \\
		\hline
		1 & 15.83 \\
		\hline
		2 & 25.87 \\
		\hline
		3 & 40.63 \\
		\hline
		4 & 60.80 \\
		\hline
		5 & 78.53 \\
		\hline
		6 & 92.97 \\
		\hline
	\end{tabular}
	\caption{Tabela przedstawia odsetek udanych ataków double spend w~zależności od~początkowej przewagi adwersarza w~wydobytych blokach względem uczciwych węzłów sieci.}
	\label{adversary-advantage-plot-results}
\end{table}

\newpage
Kolejna seria eksperymentów pokazuje wpływ mocy obliczeniowej, którą~dysponuje adwersarz na~atak double spend. Uwzględnione w~symulacji wartości mocy obliczeniowej zamieszczono w~tabelach pod wykresami. Poza modelem Erd\H os - R\'enyi symulacja została wykonana również dla~topologii linii oraz~grafu pełnego. W~celu porównania topologii wyniki zaprezentowano na~jednym wykresie. Eksperymenty zostały powtórzone dla~różnych czasów transmisji bloku $t_1 = \frac{1}{200}$, $t_2 = \frac{1}{20}$, $t_3 = \frac{1}{2}$ oraz~maksymalnego opóźnienia w~czasie transmisji $d = 0$.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{plots/adversary-power-plot-t-1-200}
	\caption{Powodzenie ataku double spend w~zależności od ilości mocy obliczeniowej będącej w~posiadaniu adwersarza. Czas transmisji bloku pomiędzy węzłami wynosi $t_1 = \frac{1}{200}$, maksymalne opóźnienie w~czasie transmisji $d = 0$.}
	\label{adversary-power-plot-t-1-200}
\end{figure}

\begin{table}[H]
	\centering
	\small	
	\begin{tabular}{| c | c | c | c |}
		\hline
		\multirow{2}{*}{\textbf{Moc adwersarza [\%]}} & \multicolumn{3}{c|}{\textbf{Liczba udanych ataków [\%]}} \\
		\cline{2-4}
		& $\mathbf{P_{30}}$ & $\mathbf{G_{(30, 0.125)}}$ & $\mathbf{K_{30}}$ \\
		\hline
		10 & 0.00 & 0.00 & 0.00 \\
		\hline
		15 & 0.20 & 0.13 & 0.20 \\
		\hline
		20 & 1.27 & 1.10 & 0.67 \\
		\hline
		25 & 3.10 & 3.00 & 2.93 \\
		\hline
		30 & 10.07 & 8.77 & 8.53 \\
		\hline
		35 & 20.83 & 18.83 & 18.17 \\
		\hline
		40 & 34.60 & 35.93 & 33.60 \\
		\hline
		45 & 56.10 & 55.20 & 53.83 \\
		\hline
		50 & 74.33 & 71.87 & 71.53 \\
		\hline
	\end{tabular}
	\caption{Tabela przedstawia odsetek udanych ataków double spend w~zależności od~ilości mocy obliczeniowej będącej w~posiadaniu adwersarza. Czas transmisji bloku pomiędzy węzłami wynosi $t_1 = \frac{1}{200}$.}
	\label{adversary-power-plot-t-1-200-results}
\end{table}

Może się wydawać, że~gdy~adwersarz dysponuje połową łącznej mocy obliczeniowej sieci, to~powodzenie ataku double spend powinno wynosić 50\%. Rysunek \ref{adversary-power-plot-t-1-200} przeczy temu przypuszczeniu, wskazując ponad 70\%. Wpływ na~wynik mają dwa czynniki. Po~pierwsze prawdopodobieństwo powodzenia ataku zwiększa czas potrzebny na~transmisję bloku pomiędzy uczciwymi węzłami, który~określa parametr $t = \frac{1}{200}$. Jednak decydującą rolę odgrywa fakt, że~adwersarzowi wystarczy jeden blok przewagi aby wygrać wyścig. Uczciwy górnicy muszą natomiast zyskać przewagę $w$ bloków by~odnieść sukces.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{plots/adversary-power-plot-t-1-20}
	\caption{Powodzenie ataku double spend w~zależności od ilości mocy obliczeniowej będącej w~posiadaniu adwersarza. Czas transmisji bloku pomiędzy węzłami wynosi $t_2 = \frac{1}{20}$, maksymalne opóźnienie czasu transmisji $d = 0$.}
	\label{adversary-power-plot-t-1-20}
\end{figure}

\begin{table}[H]
	\centering
	\small	
	\begin{tabular}{| c | c | c | c |}
		\hline
		\multirow{2}{*}{\textbf{Moc adwersarza [\%]}} & \multicolumn{3}{c|}{\textbf{Liczba udanych ataków [\%]}} \\
		\cline{2-4}
		& $\mathbf{P_{30}}$ & $\mathbf{G_{(30, 0.125)}}$ & $\mathbf{K_{30}}$ \\
		\hline
		10 & 0.07 & 0.00 & 0.07 \\
		\hline
		15 & 0.37 & 0.17 & 0.40 \\
		\hline
		20 & 2.57 & 1.00 & 0.97 \\
		\hline
		25 & 6.27 & 5.00 & 4.20 \\
		\hline
		30 & 17.67 & 11.83 & 8.33 \\
		\hline
		35 & 33.33 & 23.50 & 19.63 \\
		\hline
		40 & 55.57 & 38.67 & 38.33 \\
		\hline
		45 & 74.40 & 59.27 & 57.43 \\
		\hline
		50 & 87.80 & 75.23 & 74.03 \\
		\hline
	\end{tabular}
	\caption{Tabela przedstawia odsetek udanych ataków double spend w~zależności od~ilości mocy obliczeniowej będącej w~posiadaniu adwersarza. Czas transmisji bloku pomiędzy węzłami wynosi $t_2 = \frac{1}{20}$.}
	\label{adversary-power-plot-t-1-20-results}
\end{table}



\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{plots/adversary-power-plot-t-1-2}
	\caption{Powodzenie ataku double spend w~zależności od ilości mocy obliczeniowej będącej w~posiadaniu adwersarza.  Czas transmisji bloku pomiędzy węzłami wynosi $t_3 = \frac{1}{2}$, maksymalne opóźnienie czasu transmisji $d = 0$.}
	\label{adversary-power-plot-t-1-2}
\end{figure}

\vspace{5 cm}

\begin{table}[H]
	\centering
	\small	
	\begin{tabular}{| c | c | c | c |}
		\hline
		\multirow{2}{*}{\textbf{Moc adwersarza [\%]}} & \multicolumn{3}{c|}{\textbf{Liczba udanych ataków [\%]}} \\
		\cline{2-4}
		& $\mathbf{P_{30}}$ & $\mathbf{G_{(30, 0.125)}}$ & $\mathbf{K_{30}}$ \\
		\hline
		10 & 0.83 & 0.27 & 0.00 \\
		\hline
		15 & 9.20 & 1.47 & 0.30 \\
		\hline
		20 & 31.80 & 6.97 & 1.83 \\
		\hline
		25 & 63.23 & 19.53 & 7.73 \\
		\hline
		30 & 87.23 & 40.47 & 18.47 \\
		\hline
		35 & 95.93 & 66.87 & 34.93 \\
		\hline
		40 & 98.67 & 83.20 & 55.27 \\
		\hline
		45 & 99.80 & 93.97 & 76.13 \\
		\hline
		50 & 99.90 & 97.93 & 88.83 \\
		\hline
	\end{tabular}
	\caption{Tabela przedstawia odsetek udanych ataków double spend w~zależności od~ilości mocy obliczeniowej będącej w~posiadaniu adwersarza. Czas transmisji bloku pomiędzy węzłami wynosi $t_3 = \frac{1}{2}$.}
	\label{adversary-power-plot-t-1-2-results}
\end{table}

\newpage

Prezentowana poniżej seria eksperymentów skupia się na~zależności pomiędzy powodzeniem ataku double spend, a~czasem transmisji bloku $t$ pomiędzy węzłami sieci. Uwzględnione w~symulacji czasy transmisji zamieszczono w~tabelach z~dokładnymi wynikami. W~tym przypadku również przetestowane zostały trzy uprzednio wymienione topologie sieci. Ponadto każdy eksperyment wykonano dla~różnych mocy adwersarza, odpowiednio: 15\%, 30\% i~50\%. Opóźnienie w~czasie transmisji wynosi $d = 0$.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{plots/network-latency-hashrate-15-plot}
	\caption{Powodzenie ataku double spend w~zależności od czasu transmisji bloku $t$ pomiędzy węzłami sieci. Moc adwersarza wynosi 15\%, opóźnienia w~czasie transmisji zostały pominięte, stąd $d = 0$.}
	\label{network-latency-hashrate-15-plot}
\end{figure}

\begin{table}[H]
	\centering
	\small
	\begin{tabular}{| c | c | c | c |} 
 		\hline
 		\multirow{2}{*}{\textbf{Czas transmisji bloku t}} & \multicolumn{3}{c|}{\textbf{Liczba udanych ataków [\%]}} \\
		\cline{2-4}
		& $\mathbf{P_{30}}$ & $\mathbf{G_{(30, 0.125)}}$ & $\mathbf{K_{30}}$ \\
		\hline
		0.00 & 0.11 & 0.24 & 0.09 \\
		\hline
		0.05 & 0.38 & 0.22 & 0.24 \\
		\hline
		0.10 & 0.73 & 0.44 & 0.07 \\
		\hline
		0.15 & 1.80 & 0.44 & 0.18 \\
		\hline
		0.20 & 2.11 & 0.60 & 0.20 \\
		\hline
		0.25 & 3.18 & 0.89 & 0.36 \\
		\hline
		0.30 & 4.27 & 1.47 & 0.29 \\
		\hline
		0.35 & 4.78 & 1.73 & 0.38 \\
		\hline
		0.40 & 6.22 & 1.64 & 0.29 \\
		\hline
		0.45 & 8.20 & 2.38 & 0.53 \\
		\hline
		0.50 & 8.96 & 2.84 & 0.36 \\
		\hline
	\end{tabular}
	\caption{Tabela przedstawia odsetek udanych ataków double spend w~zależności od~czasu transmisji bloku pomiędzy węzłami sieci. Moc adwersarza wynosi 15\%}
	\label{network-latency-hashrate-15-plot-results}
\end{table}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{plots/network-latency-hashrate-30-plot}
	\caption{Powodzenie ataku double spend w~zależności od czasu transmisji bloku $t$ pomiędzy węzłami sieci. Moc adwersarza wynosi 30\%, opóźnienia w~czasie transmisji zostały pominięte, stąd $d = 0$.}
	\label{network-latency-hashrate-30-plot}
\end{figure}

\begin{table}[H]
	\centering
	\small
	\begin{tabular}{| c | c | c | c |} 
 		\hline
 		\multirow{2}{*}{\textbf{Czas transmisji bloku t}} & \multicolumn{3}{c|}{\textbf{Liczba udanych ataków [\%]}} \\
		\cline{2-4}
		& $\mathbf{P_{30}}$ & $\mathbf{G_{(30, 0.125)}}$ & $\mathbf{K_{30}}$ \\
		\hline
		0.00 & 8.60 & 8.70 & 8.80 \\
		\hline
		0.05 & 18.33 & 11.70 & 9.07 \\
		\hline
		0.10 & 27.47 & 14.17 & 10.93 \\
		\hline
		0.15 & 40.97 & 17.40 & 9.97 \\
		\hline
		0.20 & 50.53 & 19.20 & 12.17 \\
		\hline
		0.25 & 58.47 & 23.50 & 13.07 \\
		\hline
		0.30 & 68.10 & 27.97 & 13.90 \\
		\hline
		0.35 & 73.80 & 32.13 & 15.83 \\
		\hline
		0.40 & 79.70 & 34.70 & 16.87 \\
		\hline
		0.45 & 83.83 & 41.40 & 16.73 \\
		\hline
		0.50 & 85.67 & 45.07 & 17.67 \\
		\hline
	\end{tabular}
	\caption{Tabela przedstawia odsetek udanych ataków double spend w~zależności od~czasu transmisji bloku pomiędzy węzłami sieci. Moc adwersarza wynosi 30\%.}
	\label{network-latency-hashrate-30-plot-results}
\end{table}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{plots/network-latency-hashrate-50-plot}
	\caption{Powodzenie ataku double spend w~zależności od czasu transmisji bloku $t$ pomiędzy węzłami sieci. Moc adwersarza wynosi 50\%, opóźnienia w~czasie transmisji zostały pominięte, stąd $d = 0$.}
	\label{network-latency-hashrate-50-plot}
\end{figure}


\vspace{5cm}

\begin{table}[H]
	\centering
	\small
	\begin{tabular}{| c | c | c | c |} 
 		\hline
 		\multirow{2}{*}{\textbf{Czas transmisji bloku t}} & \multicolumn{3}{c|}{\textbf{Liczba udanych ataków [\%]}} \\
		\cline{2-4}
		& $\mathbf{P_{30}}$ & $\mathbf{G_{(30, 0.125)}}$ & $\mathbf{K_{30}}$ \\
		\hline
		0.00 & 73.23 & 72.67 & 72.50 \\
		\hline
		0.05 & 88.47 & 78.10 & 74.40 \\
		\hline
		0.10 & 95.03 & 82.70 & 74.87 \\
		\hline
		0.15 & 97.97 & 86.50 & 78.63 \\
		\hline
		0.20 & 99.03 & 89.90 & 79.43 \\
		\hline
		0.25 & 99.60 & 92.20 & 80.97 \\
		\hline
		0.30 & 99.56 & 95.03 & 84.30 \\
		\hline
		0.35 & 99.77 & 95.63 & 84.63 \\
		\hline
		0.40 & 99.87 & 96.73 & 86.60 \\
		\hline
		0.45 & 100.00 & 98.00 & 87.73 \\
		\hline
		0.50 & 99.97 & 98.00 & 90.30 \\
		\hline
	\end{tabular}
	\caption{Tabela przedstawia odsetek udanych ataków double spend w~zależności od~czasu transmisji bloku pomiędzy węzłami sieci. Moc adwersarza wynosi 50\%.}
	\label{network-latency-hashrate-50-plot-results}
\end{table}

\newpage

Ostatni eksperyment bada wpływ maksymalnego opóźnienia $d$ w~czasie transmisji bloku pomiędzy węzłami sieci. Ustawiony został w~nim czas transmisji bloku $t_2 = \frac{1}{20}$, a~przydzielona adwersarzowi moc wynosi 30\%. Ponownie powtórzono symulację dla~wcześniej rozważanych topologii sieci.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{plots/network-latency-deviation-hashrate-30-plot}
	\caption{Powodzenie ataku double spend w~zależności od~maksymalnego opóźnienia $d$ w~czasie transmisji bloku pomiędzy węzłami sieci. Przyjęto, że~adwersarz dysponuje łączną mocą obliczeniową na~poziomie 30\%, a~czas transmisji bloku jest równy $t_2 = \frac{1}{20}$.}
	\label{network-latency-deviation-hashrate-30-plot}
\end{figure}


\vspace{2cm}

\begin{table}[H]
	\centering
	\small
	\begin{tabular}{| c | c | c | c |} 
 		\hline
 		\multirow{2}{*}{\textbf{Maksymalne opóźnienie w~transmisji d}} & \multicolumn{3}{c|}{\textbf{Liczba udanych ataków [\%]}} \\
		\cline{2-4}
		& $\mathbf{P_{30}}$ & $\mathbf{G_{(30, 0.125)}}$ & $\mathbf{K_{30}}$ \\
		\hline
		0.00 & 16.78 & 14.48 & 9.46 \\
		\hline
		0.01 & 20.08 & 14.38 & 9.34 \\
		\hline
		0.02 & 19.18 & 13.66 & 9.82 \\
		\hline
		0.03 & 20.76 & 14.84 & 9.78 \\
		\hline
		0.04 & 21.62 & 15.68 & 9.66 \\
		\hline
		0.05 & 22.90 & 15.02 & 9.70 \\
		\hline
		0.06 & 24.28 & 15.96 & 10.10 \\
		\hline
		0.07 & 25.32 & 14.96 & 10.02 \\
		\hline
		0.08 & 25.62 & 15.22 & 10.06 \\
		\hline
		0.09 & 27.32 & 15.94 & 10.60 \\
		\hline
		0.10 & 27.46 & 16.70 & 9.84 \\
		\hline
	\end{tabular}
	\caption{Tabela przedstawia odsetek udanych ataków double spend w~zależności od~maksymalnego opóźnienia $d$ w~czasie transmisji bloku pomiędzy węzłami sieci.}
	\label{network-latency-deviation-hashrate-30-plot-results}
\end{table}

\newpage

\section{Wnioski z symulacji}
Zaczniemy od~wniosku, który~podsumowuje wynik pierwszego eksperymentu, przedstawiony na~histogramie \ref{erdos-renyi-winning-blocks-histogram}.

\begin{corollary}
Prawdopodobieństwo powodzenia ataku double spend maleje wykładniczo względem liczby potwierdzeń dla rozpatrywanej transakcji.
\end{corollary}

Eksperyment potwierdza teoretyczną analizę zamieszczoną w~rozdziale \ref{double-spend-analysis}.
Warto zauważyć, że~jeżeli adwersarzowi nie udało się przeprowadzić ataku po~wydobyciu dwudziestu bloków, to~powinien on~zrezygnować z~dalszej pracy nad~alternatywną gałęzią łańcucha, ponieważ prawdopodobieństwo odniesienia sukcesu jest bliskie zeru.

Drugi wniosek został wyciągnięty na~podstawie eksperymentu badającego wpływ strategii samolubnego wydobywania bloków na~atak double spend, jego wyniki zamieszczono na~wykresie \ref{erdos-renyi-adversary-advantage-plot}.

\begin{corollary}
Prawdopodobieństwo powodzenia ataku dobule spend wzrasta wykładniczo względem początkowej przewagi adwersarza w~wydobytych blokach nad uczciwymi węzłami sieci.
\end{corollary}

Okazuje się, że~strategia samolubnego wydobywania bloków daje bardzo dobre rezultaty. Najsilniejsze kopalnie górników, które~posiadają ponad 20\% całkowitej mocy obliczeniowej sieci znajdują 2 lub~3 bloki z~rzędu kilkukrotnie w~ciągu jednego dnia. Jeśli kopalnia dysponuje blisko jedną trzecią całkowitej mocy obliczeniowej i~udało jej się wydobyć trzy bloki z~rzędu to~prawdopodobieństwo powodzenia ataku jest większe niż 0.4.

Kolejny wniosek płynie z~serii eksperymentów, w~której zbadany został wpływ mocy obliczeniowej posiadanej przez adwersarza na~przeprowadzenie ataku double spend.

\begin{corollary}
Prawdopodobieństwo powodzenia ataku double spend wzrasta wykładniczo względem mocy obliczeniowej będącej w~posiadaniu adwersarza.
\end{corollary}

Bez stosowania strategii samolubnego wydobywania bloków realna szansa wykonania ataku double spend zachodzi w~sytuacji, gdy~adwersarz dysponuje około jedną trzecią całkowitej mocy obliczeniowej.
Zauważmy, że~topologia sieci nie~ma większego znaczenia kiedy czasy transmisji bloku są małe
np. $t_1 = \frac{1}{200}$ (zobacz rysunek \ref{adversary-power-plot-t-1-200}). Dopiero dla~wyższych czasów transmisji, np. $t_2 = \frac{1}{20}$ i~$t_3 = \frac{1}{2}$ można zauważyć istotne rozwarstwienie wykresów (odpowiednio rysunki \ref{adversary-power-plot-t-1-20} i~\ref{adversary-power-plot-t-1-2}).

Następny wniosek sformułowano na~bazie serii eksperymentów, w~której~pod uwagę wzięte zostały
różne czasy transmisji bloków w~sieci $t$.

\begin{corollary}
Prawdopodobieństwo powodzenia ataku double spend jest liniowo zależne od~czasu transmisji bloku pomiędzy węzłami sieci. 
\end{corollary}

Jeśli bierzemy pod uwagę wpływ czasu pojedynczej transmisji na powodzenie ataku, to~w oczywisty sposób topologia sieci odgrywa istotną rolę. Szczególnie w~przypadku topologii liniowej czas transmisji widocznie wpływa na wynik ataku (zobacz np. rysunek \ref{network-latency-hashrate-15-plot}).  Gdy adwersarz dysponuje mocą obliczeniową na~poziomie 15\%, to~odsetek udanych ataków dla~topologii grafu pełnego jest bliski zeru, nawet dla~bardzo wysokich wartości parametru $t$ (zobacz rysunek \ref{network-latency-hashrate-15-plot}). Eksperyment, w~którym~adwersarz posiada połowę całkowitej mocy obliczeniowej pokazuje, że~topologia sieci przestaje być czynnikiem wywierającym dominujący wpływ na~powodzenie ataku. Dzieje się tak, ponieważ wyniki dla~topologii liniowej i~modelu Erd\H os - R\'enyi szybko zbiegają do~100\% sukcesów atakującego (zobacz \ref{network-latency-hashrate-50-plot}).

Ostatni z~przedstawionych eksperymentów prowadzi do~wniosku na~temat wpływu maksymalnej wartości opóźnienia transmisji $d$ na~wynik ataku. Moc atakującego wynosiła 30\%. Wynik symulacji został zamieszczony na~rysunku \ref{network-latency-deviation-hashrate-30-plot}.
Odsetek sukcesów adwersarza stopniowo wzrasta dla~topologii liniowej wraz z~rosnącymi wartościami maksymalnego opóźnienia transmisji $d$. Zależność ma~charakter liniowy (chociaż na wykresie zaobserwować można duże wahania wyników).
Wynik eksperymentu pokazuje, że~zaburzenia w~czasie transmisji mają  mały wpływ na sukces ataku w przypadku topologii grafu pełnego oraz dla modelu Erd\H os - R\'enyi. W~tym przypadku prawdopodobieństwo odniesienia sukcesu przez~atakującego oscyluje wokół stałej wartości.
