from functools import total_ordering


@total_ordering
class Event(object):
    def __init__(self, time, owner, block_number, recipient=None):
        self._time = time
        self._owner = owner
        self._block_number = block_number
        self._recipient = recipient

    @property
    def time(self):
        return self._time

    @property
    def owner(self):
        return self._owner

    @property
    def block_number(self):
        return self._block_number

    @property
    def recipient(self):
        return self._recipient

    def is_new_block_event(self):
        return self.recipient is None

    def convert_recipient(self):
        return self._recipient if self._recipient else -1

    def __lt__(self, other):
        return (self.time, self._owner, self._block_number, self.convert_recipient()) \
               < (other.time, other.owner, other.block_number, other.convert_recipient())

    def __eq__(self, other):
        return (self.time, self._owner, self._block_number, self.convert_recipient()) \
               == (other.time, other.owner, other.block_number, other.convert_recipient())

    def __str__(self):
        return "[time={0}, owner={1}, block_number={2}, recipient={3}]" \
            .format(self._time, self._owner, self._block_number, self._recipient)

    def __repr__(self):
        return self.__str__()
