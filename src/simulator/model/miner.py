from heapq import heappush

from numpy.random.mtrand import uniform

from simulator.model.adversary import Adversary
from simulator.model.event import Event


class Miner(Adversary):
    def __init__(self, id_, mining_power, peers=[], current_block_number=0, current_block_owner=None):
        Adversary.__init__(self, mining_power, current_block_number, id_, current_block_owner)
        self._peers = peers

    @property
    def peers(self):
        return self._peers

    def propagate_current_block(self, miners, current_time, event_queue, network_latency, network_latency_deviation):
        for peer in self._peers:
            if self._current_block_number > miners[peer].current_block_number:
                propagation_time = current_time + network_latency + uniform(0, network_latency_deviation)
                propagation_event = Event(propagation_time, self._current_block_owner, self._current_block_number, peer)
                heappush(event_queue, propagation_event)

    def reset_blockchain(self):
        self.current_block_number = 0
        self._current_block_owner = None

    def __str__(self):
        return "[id={0}, power={1}, peers={2}, current_block_number={3}, owner={4}]" \
            .format(self._id_, self._mining_power, self._peers, self._current_block_number, self._current_block_owner)

    def __repr__(self):
        return self.__str__()
