from heapq import heappush

from numpy.random.mtrand import exponential

from simulator.configuration.input import average_time_to_mine_block, malicious_branch_initial_advantage
from simulator.model.event import Event

ADVERSARY_ID = 0


class Adversary(object):
    def __init__(self, mining_power, current_block_number=malicious_branch_initial_advantage, id_=ADVERSARY_ID,
                 current_block_owner=None):
        self._id_ = id_
        self._mining_power = mining_power
        self._current_block_number = current_block_number
        self._current_block_owner = current_block_owner

    @property
    def id_(self):
        return self._id_

    @property
    def mining_power(self):
        return self._mining_power

    @mining_power.setter
    def mining_power(self, value):
        self._mining_power = value

    @property
    def current_block_number(self):
        return self._current_block_number

    @current_block_number.setter
    def current_block_number(self, value):
        self._current_block_number = value

    @property
    def current_block_owner(self):
        return self._current_block_owner

    def propagate_current_block(self, miners, current_time, event_queue, network_latency, network_latency_deviation):
        pass

    def update_blockchain(self, block_number, owner):
        self._current_block_number = block_number
        self._current_block_owner = owner

    def mine_new_block(self, current_time, event_queue):
        mining_time = current_time + exponential(average_time_to_mine_block / self._mining_power)
        new_block_event = Event(mining_time, self._id_, self._current_block_number + 1)
        heappush(event_queue, new_block_event)

    def reset_blockchain(self):
        self._current_block_number = malicious_branch_initial_advantage
        self._current_block_owner = None

    def __str__(self):
        return "[id={0}, power={1}, current_block_number={2}, owner={3}]" \
            .format(self._id_, self._mining_power, self._current_block_number, self._current_block_owner)

    def __repr__(self):
        return self.__str__()
