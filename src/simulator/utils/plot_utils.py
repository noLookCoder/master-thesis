import sys
from collections import Counter

import matplotlib.pyplot as plt
import networkx as nx
from matplotlib.ticker import MaxNLocator
from numpy import where, arange

AXIS_OFFSET = 0.5


class PlotUtils(object):
    @staticmethod
    def draw_plot(x_data, p_data, er_data, k_data, x_label, y_label):
        plt.cla()
        plt.plot(x_data, p_data, label="Linia")
        plt.plot(x_data, er_data, label="Erdos-Renyi")
        plt.plot(x_data, k_data, label="Graf pełny")
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.legend()
        PlotUtils._print_exact_results_table_for_plot(x_data, p_data, er_data, k_data)
        plt.show()

    @staticmethod
    def draw_scatter(x_data, y_data, x_label, y_label):
        plt.cla()
        plt.scatter(x_data, y_data, s=16)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        PlotUtils._print_exact_results_table_for_scatter(x_data, y_data)
        plt.show()

    @staticmethod
    def draw_histogram(data, x_label, y_label):
        plt.cla()
        plt.hist(data, bins=arange(min(data), max(data) + 2) - AXIS_OFFSET)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        x1, x2, y1, y2 = plt.axis()
        plt.axis((x1 - AXIS_OFFSET, x2 + AXIS_OFFSET, y1, y2 + 0.05))
        axes = plt.gca()
        axes.xaxis.set_major_locator(MaxNLocator(integer=True))
        PlotUtils._print_exact_results_table_for_hist(data)
        plt.show()

    @staticmethod
    def draw_topology(adjacency_matrix, node_labels):
        rows, cols = where(adjacency_matrix == 1)
        edges = zip(rows.tolist(), cols.tolist())
        graph = nx.Graph()
        graph.add_edges_from(edges)
        try:
            nx.draw_circular(graph, node_size=250, labels=node_labels)
        except KeyError:
            print("Generated topology contains isolated nodes.\nPlease change configuration or run simulation again.")
            sys.exit(0)
        plt.axis("equal")
        plt.show()

    @staticmethod
    def _print_exact_results_table_for_scatter(x_data, y_data):
        for x, y in zip(x_data, y_data):
            print("{0} & {1} \\\\\n\\hline".format(x, y))

    @staticmethod
    def _print_exact_results_table_for_plot(x_data, p_data, er_data, k_data):
        for i, j, k, l in zip(x_data, p_data, er_data, k_data):
            print("{0} & {1} & {2} & {3} \\\\\n\\hline".format(i, j, k, l))

    @staticmethod
    def _print_exact_results_table_for_hist(data):
        counter = Counter(data)
        for x, y in counter.items():
            print("{0} & {1} \\\\\n\\hline".format(x, y))
