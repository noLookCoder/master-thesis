from random import randint, sample, uniform

from simulator.configuration.configuration_parser import OFFSET

SECOND_NODE = 2


class TopologyGenerator(object):
    @staticmethod
    def generate_line_topology(nodes_number):
        network_topology = [[(node - OFFSET) % nodes_number + OFFSET, (node + OFFSET) % nodes_number + OFFSET] for node
                            in range(OFFSET, nodes_number - OFFSET)]
        network_topology.insert(0, [SECOND_NODE])
        network_topology.append([nodes_number - OFFSET])

        return network_topology

    @staticmethod
    def generate_full_topology(nodes_number):
        network_topology = [[(node + peer + OFFSET) % nodes_number + OFFSET for peer in range(nodes_number - OFFSET)]
                            for node in range(nodes_number)]

        return network_topology

    @staticmethod
    def generate_erdos_renyi_topology(nodes_number, edge_probability):
        network_topology = [[] for _ in range(nodes_number)]
        for i in range(nodes_number):
            for j in range(i + OFFSET, nodes_number):
                if edge_probability >= uniform(0, 1):
                    network_topology[i % nodes_number].append(j % nodes_number + OFFSET)
                    network_topology[j % nodes_number].append(i % nodes_number + OFFSET)

        return network_topology

    @staticmethod
    def generate_random_topology(nodes_number, peers_number, peers_number_deviation):
        network_topology = []
        for _ in range(nodes_number):
            population = peers_number + randint(-peers_number_deviation, peers_number_deviation)
            peers = sample(range(OFFSET, nodes_number + OFFSET), population)
            network_topology.append(peers)

        return network_topology

    @staticmethod
    def generate_ring_topology(nodes_number):
        network_topology = [[(node + OFFSET) % nodes_number + OFFSET] for node in range(nodes_number)]

        return network_topology

    @staticmethod
    def generate_honest_nodes_mining_powers(nodes_number, adversary_mining_power):
        honest_nodes_mining_power = 1 - adversary_mining_power
        return [honest_nodes_mining_power / nodes_number] * nodes_number
