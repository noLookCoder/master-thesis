from numpy import arange, log, linspace

from simulator.configuration.configuration_parser import ConfigurationParser
from simulator.configuration.input import experiment_repeats
from simulator.double_spend_simulator import DoubleSpendAttackSimulator
from simulator.model.adversary import ADVERSARY_ID
from simulator.utils.plot_utils import PlotUtils
from simulator.utils.topology_generator import TopologyGenerator

ADVERSARY_BRANCH_INITIAL_ADVANTAGE = "Długość gałęzi adwersarza względem uczciwych węzłów"
SUCCESSFUL_DOUBLE_SPENDS = "Liczba udanych ataków double spend"
BLOCK_NUMBER = "Numer  bloku"
ADVERSARY_POWER = "Moc adwersarza [%]"
TRANSMISSION_TIME = "Czas transmisji bloku t"
TRANSMISSION_DEVIATION = "Maksymalne opóźnienie w transmisji d"
DOUBLE_SPEND_SUCCESS_RATE = SUCCESSFUL_DOUBLE_SPENDS + " [%]"

NODES_NUMBER = 30
PEERS_NUMBER = 3
PEERS_NUMBER_DEVIATION = 0

ADVERSARY_MIN_POWER = 0.1
ADVERSARY_MAX_POWER = 0.5
ADVERSARY_SMALL_POWER = 0.15
ADVERSARY_DEFAULT_POWER = 0.3
ADVERSARY_BIG_POWER = 0.5
POWER_STEP = 0.05

NETWORK_MIN_LATENCY = 1 / 400
NETWORK_MAX_LATENCY = 1 / 2
NETWORK_DEFAULT_LATENCY = 1 / 200
NETWORK_BIG_LATENCY = 1 / 20
NETWORK_HUGE_LATENCY = 1 / 2
LATENCY_SAMPLES = 11

NETWORK_DEFAULT_LATENCY_DEVIATION = 0
NETWORK_MIN_LATENCY_DEVIATION = 1 / 800
NETWORK_MAX_LATENCY_DEVIATION = 1 / 10

ADVERSARY_MIN_ADVANTAGE = -4
ADVERSARY_MAX_ADVANTAGE = 6
ADVERSARY_DEFAULT_ADVANTAGE = 0
ADVANTAGE_STEP = 1

PERCENT_MULTIPLIER = 100
ATTACK_SUCCEEDS = 1000
EDGE_OCCURRENCE_PROBABILITY = 1.1 * log(30) / 30


def prepare_simulator(configuration_parser):
    miners = configuration_parser.get_miners()
    simulator = DoubleSpendAttackSimulator(miners, latency=NETWORK_DEFAULT_LATENCY)

    return simulator


def draw_topology(configuration_parser):
    adjacency_matrix = configuration_parser.convert_adjacency_list_to_matrix()
    node_labels = configuration_parser.get_node_labels()
    PlotUtils.draw_topology(adjacency_matrix, node_labels)


def count_double_spend_successes_depending_on_block_number(simulator):
    double_spends = 0
    adversary_winning_blocks = []
    attack_succeeds = ATTACK_SUCCEEDS
    while double_spends < attack_succeeds:
        double_spend_result, malicious_branch_length = simulator.run_simulation()
        if double_spend_result:
            double_spends += 1
            adversary_winning_blocks.append(malicious_branch_length)
            print(double_spends)
        simulator.reset_state()

    PlotUtils.draw_histogram(adversary_winning_blocks, BLOCK_NUMBER, SUCCESSFUL_DOUBLE_SPENDS)


def examine_adversary_mining_power_influence_on_double_spend_success_rate(simulator):
    adversary_mining_powers = []
    attack_results = []
    for adversary_power in arange(ADVERSARY_MIN_POWER, ADVERSARY_MAX_POWER + POWER_STEP, POWER_STEP):
        adjust_mining_powers(adversary_power, simulator)
        double_spends = 0
        for i in range(experiment_repeats):
            double_spend_result, _ = simulator.run_simulation()
            if double_spend_result:
                double_spends += 1
                print(adversary_power, i)
            simulator.reset_state()
        adversary_mining_powers.append(adversary_power * PERCENT_MULTIPLIER)
        attack_results.append((double_spends / experiment_repeats) * PERCENT_MULTIPLIER)

    adjust_mining_powers(ADVERSARY_DEFAULT_POWER, simulator)
    return adversary_mining_powers, attack_results


def adjust_mining_powers(adversary_power, simulator):
    mining_powers = [adversary_power]
    mining_powers.extend(
        TopologyGenerator.generate_honest_nodes_mining_powers(NODES_NUMBER, adversary_power))
    for miner, mining_power in zip(simulator.miners, mining_powers):
        miner.mining_power = mining_power


def examine_adversary_initial_advantage_influence_on_double_spend_success_rate(simulator):
    adversary_initial_advantages = []
    attack_results = []
    for adversary_advantage in range(ADVERSARY_MIN_ADVANTAGE, ADVERSARY_MAX_ADVANTAGE + ADVANTAGE_STEP, ADVANTAGE_STEP):
        simulator.malicious_branch_advantage = adversary_advantage
        simulator.miners[ADVERSARY_ID].current_block_number = adversary_advantage
        double_spends = 0
        for i in range(experiment_repeats):
            double_spend_result, _ = simulator.run_simulation()
            if double_spend_result:
                double_spends += 1
                print(adversary_advantage, i)
            simulator.reset_state()
            simulator.malicious_branch_advantage = adversary_advantage
            simulator.miners[ADVERSARY_ID].current_block_number = adversary_advantage
        adversary_initial_advantages.append(adversary_advantage)
        attack_results.append((double_spends / experiment_repeats) * PERCENT_MULTIPLIER)

    simulator.malicious_branch_advantage = ADVERSARY_DEFAULT_ADVANTAGE
    simulator.miners[ADVERSARY_ID].current_block_number = ADVERSARY_DEFAULT_ADVANTAGE
    PlotUtils.draw_scatter(adversary_initial_advantages, attack_results,
                           ADVERSARY_BRANCH_INITIAL_ADVANTAGE,
                           DOUBLE_SPEND_SUCCESS_RATE)


def examine_network_latency_influence_on_double_spend_success_rate(simulator):
    network_latencies = []
    attack_results = []
    for network_latency in linspace(NETWORK_MIN_LATENCY, NETWORK_MAX_LATENCY, LATENCY_SAMPLES):
        simulator.network_latency = network_latency
        double_spends = 0
        for i in range(experiment_repeats):
            double_spend_result, _ = simulator.run_simulation()
            if double_spend_result:
                double_spends += 1
                print(network_latency, i)
            simulator.reset_state()
        network_latencies.append(network_latency)
        attack_results.append((double_spends / experiment_repeats) * PERCENT_MULTIPLIER)

    simulator.network_latency = NETWORK_DEFAULT_LATENCY
    return network_latencies, attack_results


def examine_network_latency_deviation_influence_on_double_spend_success_rate(simulator):
    network_latency_deviations = []
    attack_results = []
    for latency_deviation in linspace(NETWORK_MIN_LATENCY_DEVIATION, NETWORK_MAX_LATENCY_DEVIATION, LATENCY_SAMPLES):
        simulator.network_latency_deviation = latency_deviation
        double_spends = 0
        for i in range(experiment_repeats):
            double_spend_result, _ = simulator.run_simulation()
            if double_spend_result:
                double_spends += 1
                print(latency_deviation, i)
            simulator.reset_state()
        network_latency_deviations.append(latency_deviation)
        attack_results.append((double_spends / experiment_repeats) * PERCENT_MULTIPLIER)

    simulator.network_latency_deviation = NETWORK_DEFAULT_LATENCY_DEVIATION
    return network_latency_deviations, attack_results


def run_experiments():
    honest_nodes_mining_powers = TopologyGenerator.generate_honest_nodes_mining_powers(NODES_NUMBER,
                                                                                       ADVERSARY_DEFAULT_POWER)
    network_topology = TopologyGenerator.generate_erdos_renyi_topology(NODES_NUMBER, EDGE_OCCURRENCE_PROBABILITY)
    configuration_parser = ConfigurationParser(ADVERSARY_DEFAULT_POWER, honest_nodes_mining_powers, network_topology)
    draw_topology(configuration_parser)
    simulator = prepare_simulator(configuration_parser)
    count_double_spend_successes_depending_on_block_number(simulator)
    examine_adversary_initial_advantage_influence_on_double_spend_success_rate(simulator)
    adversary_mining_powers, er_power_results = examine_adversary_mining_power_influence_on_double_spend_success_rate(
        simulator)
    network_latencies, er_latency_results = examine_network_latency_influence_on_double_spend_success_rate(simulator)
    network_latency_deviations, er_latency_deviation_results = examine_network_latency_deviation_influence_on_double_spend_success_rate(
        simulator)

    simulator.network_latency = NETWORK_BIG_LATENCY
    _, er_power_big_latency_results = examine_adversary_mining_power_influence_on_double_spend_success_rate(
        simulator)
    simulator.network_latency = NETWORK_HUGE_LATENCY
    _, er_power_huge_latency_results = examine_adversary_mining_power_influence_on_double_spend_success_rate(
        simulator)
    simulator.network_latency = NETWORK_DEFAULT_LATENCY

    honest_nodes_big_mining_powers = TopologyGenerator.generate_honest_nodes_mining_powers(NODES_NUMBER,
                                                                                           ADVERSARY_SMALL_POWER)
    configuration_parser = ConfigurationParser(ADVERSARY_SMALL_POWER, honest_nodes_big_mining_powers, network_topology)
    simulator = prepare_simulator(configuration_parser)
    _, er_latency_big_mining_powers_results = examine_network_latency_influence_on_double_spend_success_rate(simulator)

    honest_nodes_small_mining_powers = TopologyGenerator.generate_honest_nodes_mining_powers(NODES_NUMBER,
                                                                                             ADVERSARY_BIG_POWER)
    configuration_parser = ConfigurationParser(ADVERSARY_BIG_POWER, honest_nodes_small_mining_powers, network_topology)
    simulator = prepare_simulator(configuration_parser)
    _, er_latency_small_mining_powers_results = examine_network_latency_influence_on_double_spend_success_rate(
        simulator)

    network_topology = TopologyGenerator.generate_line_topology(NODES_NUMBER)
    configuration_parser = ConfigurationParser(ADVERSARY_DEFAULT_POWER, honest_nodes_mining_powers, network_topology)
    draw_topology(configuration_parser)
    simulator = prepare_simulator(configuration_parser)
    _, p_power_results = examine_adversary_mining_power_influence_on_double_spend_success_rate(simulator)
    _, p_latency_results = examine_network_latency_influence_on_double_spend_success_rate(simulator)
    _, p_latency_deviation_results = examine_network_latency_deviation_influence_on_double_spend_success_rate(simulator)
    simulator.network_latency = NETWORK_BIG_LATENCY
    _, p_power_big_latency_results = examine_adversary_mining_power_influence_on_double_spend_success_rate(
        simulator)
    simulator.network_latency = NETWORK_HUGE_LATENCY
    _, p_power_huge_latency_results = examine_adversary_mining_power_influence_on_double_spend_success_rate(
        simulator)
    simulator.network_latency = NETWORK_DEFAULT_LATENCY

    configuration_parser = ConfigurationParser(ADVERSARY_SMALL_POWER, honest_nodes_big_mining_powers, network_topology)
    simulator = prepare_simulator(configuration_parser)
    _, p_latency_big_mining_powers_results = examine_network_latency_influence_on_double_spend_success_rate(simulator)

    configuration_parser = ConfigurationParser(ADVERSARY_BIG_POWER, honest_nodes_small_mining_powers, network_topology)
    simulator = prepare_simulator(configuration_parser)
    _, p_latency_small_mining_powers_results = examine_network_latency_influence_on_double_spend_success_rate(simulator)

    network_topology = TopologyGenerator.generate_full_topology(NODES_NUMBER)
    configuration_parser = ConfigurationParser(ADVERSARY_DEFAULT_POWER, honest_nodes_mining_powers, network_topology)
    draw_topology(configuration_parser)
    simulator = prepare_simulator(configuration_parser)
    _, k_power_results = examine_adversary_mining_power_influence_on_double_spend_success_rate(simulator)
    _, k_latency_results = examine_network_latency_influence_on_double_spend_success_rate(simulator)
    _, k_latency_deviation_results = examine_network_latency_deviation_influence_on_double_spend_success_rate(simulator)

    simulator.network_latency = NETWORK_BIG_LATENCY
    _, k_power_big_latency_results = examine_adversary_mining_power_influence_on_double_spend_success_rate(
        simulator)
    simulator.network_latency = NETWORK_HUGE_LATENCY
    _, k_power_huge_latency_results = examine_adversary_mining_power_influence_on_double_spend_success_rate(
        simulator)
    simulator.network_latency = NETWORK_DEFAULT_LATENCY

    configuration_parser = ConfigurationParser(ADVERSARY_SMALL_POWER, honest_nodes_big_mining_powers, network_topology)
    simulator = prepare_simulator(configuration_parser)
    _, k_latency_big_mining_powers_results = examine_network_latency_influence_on_double_spend_success_rate(simulator)

    configuration_parser = ConfigurationParser(ADVERSARY_BIG_POWER, honest_nodes_small_mining_powers, network_topology)
    simulator = prepare_simulator(configuration_parser)
    _, k_latency_small_mining_powers_results = examine_network_latency_influence_on_double_spend_success_rate(simulator)

    PlotUtils.draw_plot(adversary_mining_powers, p_power_results, er_power_results, k_power_results,
                        ADVERSARY_POWER, DOUBLE_SPEND_SUCCESS_RATE)
    PlotUtils.draw_plot(adversary_mining_powers, p_power_big_latency_results, er_power_big_latency_results,
                        k_power_big_latency_results, ADVERSARY_POWER, DOUBLE_SPEND_SUCCESS_RATE)
    PlotUtils.draw_plot(adversary_mining_powers, p_power_huge_latency_results, er_power_huge_latency_results,
                        k_power_huge_latency_results, ADVERSARY_POWER, DOUBLE_SPEND_SUCCESS_RATE)

    PlotUtils.draw_plot(network_latencies, p_latency_small_mining_powers_results,
                        er_latency_small_mining_powers_results, k_latency_small_mining_powers_results,
                        TRANSMISSION_TIME, DOUBLE_SPEND_SUCCESS_RATE)
    PlotUtils.draw_plot(network_latencies, p_latency_results, er_latency_results, k_latency_results,
                        TRANSMISSION_TIME, DOUBLE_SPEND_SUCCESS_RATE)
    PlotUtils.draw_plot(network_latencies, p_latency_big_mining_powers_results,
                        er_latency_big_mining_powers_results, k_latency_big_mining_powers_results,
                        TRANSMISSION_TIME, DOUBLE_SPEND_SUCCESS_RATE)

    PlotUtils.draw_plot(network_latency_deviations, p_latency_deviation_results, er_latency_deviation_results,
                        k_latency_deviation_results, TRANSMISSION_DEVIATION,
                        DOUBLE_SPEND_SUCCESS_RATE)


def run_double_spend_attack_based_on_configuration():
    configuration_parser = ConfigurationParser()
    simulator = prepare_simulator(configuration_parser)
    is_attack_succeed, _ = simulator.run_simulation()
    if is_attack_succeed:
        print("adversary win")
    else:
        print("adversary lose")


if __name__ == "__main__":
    # run_experiments()
    run_double_spend_attack_based_on_configuration()
