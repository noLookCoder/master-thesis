from numpy import zeros

from simulator.configuration.input import adversary_mining_power, honest_nodes_mining_powers, network_topology
from simulator.model.adversary import Adversary
from simulator.model.miner import Miner

OFFSET = 1
PRESENT = 1


class ConfigurationParser(object):
    def __init__(self, adversary_power=adversary_mining_power, mining_powers=honest_nodes_mining_powers,
                 topology=network_topology):
        self._adversary_mining_power = adversary_power
        self._honest_nodes_mining_powers = mining_powers
        self._network_topology = topology

    def get_miners(self):
        miners = [Adversary(self._adversary_mining_power)]
        for id_, (mining_power, peers) in enumerate(zip(self._honest_nodes_mining_powers, self._network_topology)):
            miners.append(Miner(id_ + OFFSET, mining_power, peers))

        return miners

    def convert_adjacency_list_to_matrix(self):
        matrix_size = len(self._network_topology)
        adjacency_matrix = zeros((matrix_size, matrix_size))
        for idx, peers in enumerate(self._network_topology):
            for peer in peers:
                adjacency_matrix[idx][peer - OFFSET] = PRESENT

        return adjacency_matrix

    def get_node_labels(self):
        return {label: str(label + OFFSET) for label in range(len(self._network_topology))}
