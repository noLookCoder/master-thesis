adversary_mining_power = 0.3
honest_nodes_mining_powers = [0.1, 0.1, 0.1, 0.1, 0.1, 0.2]
network_topology = [[2, 3, 4, 5, 6],
                    [1, 3, 4, 5, 6],
                    [1, 2, 4, 5, 6],
                    [1, 2, 3, 5, 6],
                    [1, 2, 3, 4, 6],
                    [1, 2, 3, 4, 5]]
network_latency_deviation = 0  # d
malicious_branch_initial_advantage = 0  # a
honest_branch_advantage_threshold = 6  # w
valid_transaction_confirmations = 6  # c
average_time_to_mine_block = 1  # u
network_latency = 1 / 200  # t
experiment_repeats = 3000
