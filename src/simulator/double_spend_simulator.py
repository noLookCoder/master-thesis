import copy
from heapq import heappop

from simulator.configuration.input import honest_branch_advantage_threshold, valid_transaction_confirmations, \
    malicious_branch_initial_advantage, network_latency, network_latency_deviation
from simulator.model.adversary import ADVERSARY_ID


class DoubleSpendAttackSimulator(object):
    def __init__(self, miners, honest_branch_length=0, malicious_branch_length=malicious_branch_initial_advantage,
                 simulation_start_time=0, latency=network_latency, deviation=network_latency_deviation):
        self._miners = miners
        self._honest_branch_length = honest_branch_length
        self._malicious_branch_length = malicious_branch_length
        self._simulation_start_time = simulation_start_time
        self._event_queue = []
        self._network_latency = latency
        self._network_latency_deviation = deviation

    @property
    def miners(self):
        return self._miners

    @property
    def malicious_branch_advantage(self):
        return self._malicious_branch_length

    @malicious_branch_advantage.setter
    def malicious_branch_advantage(self, value):
        self._malicious_branch_length = value

    @property
    def network_latency(self):
        return self._network_latency

    @network_latency.setter
    def network_latency(self, value):
        self._network_latency = value

    @property
    def network_latency_deviation(self):
        return self._network_latency_deviation

    @network_latency_deviation.setter
    def network_latency_deviation(self, value):
        self._network_latency_deviation = value

    def run_simulation(self):
        for miner in self._miners:
            miner.mine_new_block(self._simulation_start_time, self._event_queue)

        while self.is_double_spend_not_succeeded() and self.is_honest_branch_advantage_threshold_not_exceeded():
            # self._preview(self._event_queue)
            event = heappop(self._event_queue)
            if event.is_new_block_event():
                miner = self._miners[event.owner]
                if self.is_current_event(event, miner):
                    self.extend_longest_branch(event)
                    self.propagate_current_block_and_mine_new_block(miner, event)
            else:
                miner = self._miners[event.recipient]
                if self.is_current_event(event, miner):
                    self.propagate_current_block_and_mine_new_block(miner, event)

        return self._malicious_branch_length > self._honest_branch_length, self._malicious_branch_length

    def is_double_spend_not_succeeded(self):
        return self._malicious_branch_length <= self._honest_branch_length \
               or self._malicious_branch_length <= valid_transaction_confirmations

    def is_honest_branch_advantage_threshold_not_exceeded(self):
        return self._honest_branch_length - self._malicious_branch_length < honest_branch_advantage_threshold

    @staticmethod
    def is_current_event(event, miner):
        return event.block_number > miner.current_block_number

    def extend_longest_branch(self, event):
        if event.owner == ADVERSARY_ID:
            self._malicious_branch_length += 1
        elif event.block_number > self._honest_branch_length:
            self._honest_branch_length += 1

    def propagate_current_block_and_mine_new_block(self, miner, event):
        miner.update_blockchain(event.block_number, event.owner)
        miner.propagate_current_block(self._miners, event.time, self._event_queue, self._network_latency,
                                      self._network_latency_deviation)
        miner.mine_new_block(event.time, self._event_queue)

    def reset_state(self):
        self._honest_branch_length = 0
        self._malicious_branch_length = malicious_branch_initial_advantage
        self._simulation_start_time = 0
        self._event_queue = []
        for miner in self._miners:
            miner.reset_blockchain()

    def _preview(self, event_queue):
        sorted_events = []
        event_queue_copy = copy.deepcopy(event_queue)

        while event_queue_copy:
            sorted_events.append(heappop(event_queue_copy))

        for miner in self._miners:
            print(miner)
        print("honest_branch_length={0}, malicious_branch_length={1}"
              .format(self._honest_branch_length, self._malicious_branch_length))
        print("event_queue={0}\n".format(sorted_events))
