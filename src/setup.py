from setuptools import setup

setup(name="double_spend_attack_simulator",
      version="1.0",
      description="Configurable simulator of double spend attack in P2P network",
      author="Maciej Krawczyk",
      author_email="krawczyk.mcj@gmail.com",
      license="MIT",
      packages=["simulator", "simulator.configuration", "simulator.model", "simulator.utils"],
      zip_safe=False,
      install_requires=["numpy", "matplotlib", "networkx"])
