\chapter{Analiza ataku double-spend} \label{double-spend-analysis}
\thispagestyle{chapterBeginStyle}
Kluczowym problemem dla systemów kryptowalut, który przed pojawieniem się pracy Satoshi Nakamoto  \cite{bitcoin-invention} nie miał rozwiązania, jest problem podwójnego wydatkowania. 
Dotychczas w~pracy skupialiśmy się na~technicznych aspektach rozwiązania Nakamoto.
W tym rozdziale zaprezentujemy dwie matematyczne analizy związane z atakiem double-spend.
Autorem pierwszej z~nich, nieco uproszczonej,  jest Nakamoto \cite{bitcoin-invention}.
Druga analiza przeprowadzona przez  Grunspana  et al. (\cite{double-spend-races}, 2017) jest bardziej precyzyjna. 
 Mimo to, pomija pewne istotne aspekty działania rozproszonego systemu, m.in. czas potrzebny na~propagację bloków w~sieci.
  Prezentowane wyniki umożliwiają matematyczne sformułowanie problemu
 i~stanowią podstawę dla symulatora opisanego w kolejnym rozdziale.
 Symulator ten będzie brał jednak pod uwagę również dodatkowe aspekty działania systemu, które nie zostały uwzględnione w modelach matematycznych.



\section{Proces Poissona} \label{block-mining-analysis}

Przypomnijmy, że~proces wydobywania bloku polega na obliczaniu wartości funkcji skrótu dla~zawartości bloku skonkatenowanej z~różnymi wartościami \texttt{nonce}.
Celem tych obliczeń jest znalezienie skrótu posiadającego zadaną liczbę zer na~początku. Niech zmienna losowa $T$ określa czas potrzebny do~wydobycia bloku.
Jeśli przyjmiemy, że próby wyliczenia odpowiedniego skrótu dla kolejnych wartości \texttt{nonce} są~niezależne od~siebie, to rozkład prawdopodobieństwa z~którego pochodzi  zmienna $T$ posiada własność braku pamięci.
\begin{definition}
Rozkład prawdopodobieństwa posiada własność \underline{braku pamięci}, jeżeli dla zmiennej losowej $T$ pochodzącej z~tego rozkładu zachodzi własność: $$\left(\forall t_1, t_2 > 0\right)\left(\mathbb{P}[T > t_1 + t_2|T > t_2] = \mathbb{P}[T > t_1]\right)~.$$
\end{definition}
Jeśli dodatkowo przyjmiemy, że zmienna losowa $T$ ma rozkład ciągły, własność braku pamięci
determinuje, że $T$ ma rozkład wykładniczy.
Funkcja gęstości dla zmiennej o rozkładzie wykładniczym $T\sim Exp(\alpha)$
ma postać
$$f_T(t) = \alpha e^{-\alpha t}~,$$
gdzie parametr $\alpha > 0$ determinuje średni czas wydobycia bloku, który~wynosi $$\mathbb{E}[T] = \frac{1}{\alpha}.$$ 


Niech zmienna losowa $T_k\sim Exp(\alpha)$ określa czas wykorzystany na wydobycie $k$-tego bloku. Zauważmy, że $T_1, T_2, \ldots, T_n$ jest ciągiem niezależnych zmiennych losowych oraz, że
zmienna $$ S_n = \sum_{k = 1}^{n} T_k$$ ma rozkład gamma z~parametrami $(n, \alpha)$.
Zdefiniujmy proces stochastyczny $N(t)$ określający liczbę wydobytych bloków do chwili $t$. Ustalmy $S_0 = 0$, wówczas: 
$$N(t) = max\{n \geq 0 : S_n < t\}~.$$
Założenie, iż czas pomiędzy wydobyciem kolejnych bloków ma rozkład wykładniczy z parametrem $\alpha$
determinuje, że $N(t)$ musi być procesem Poissona o intensywności $\alpha$ (zobacz np. \cite{probability-and-computing}, Twierdzenie 8.11).
Zauważmy, że funkcja gęstości zmiennej losowej $S_n$ o rozkładzie gamma ma postać: 
$$f_{S_n}(t) = \frac{\alpha^n}{(n-1)!} t^{n-1}e^{-\alpha t}~.$$
Natomiast jej dystrybuanta zadana jest wzorem: 
$$F_{S_n}(t) = \int_{0}^{t} f_{S_n}(u)du = 1 - e^{-\alpha t} \sum_{k = 0}^{n - 1} \frac{(\alpha t)^k}{k!}~.$$
Jeżeli $N(t) = n$, to~$ S_n \leq t $ oraz~$ S_{n+1} > t $, stąd: 
$$\mathbb{P}[N(t) = n] = F_{S_n}(t) - F_{S_{n+1}}(t) = \frac{(\alpha t) ^ n}{n!}e^{-\alpha t}~.$$



\section{Wyścig z adwersarzem}
Rozważmy wyścig, który~zachodzi pomiędzy adwersarzem, a~uczciwymi węzłami sieci w~trakcie ataku double spend (zobacz sekcję \ref{double-spend-overview}). Obie strony starają się jak najszybciej wydobyć kolejne bloki. 
Przypiszmy zmienne losowe $T$ i~$S_n$ zdefiniowane w poprzedniej sekcji do procesu wydobywania bloków przez uczciwe węzły oraz niech analogiczne zmienne $T'$ i~$S_n'$ będą związane z wydobywaniem bloków przez ~adwersarza. Podobnie oznaczmy proces Poissona zliczający wydobyte bloki związany z~uczciwymi węzłami przez $N(t)$, a przez $N'(t)$ proces związany z adwersarzem. Zmienne losowe $T$ oraz~$T'$ są~niezależne od~siebie i~mają rozkład wykładniczy.
Przyjmijmy, że $T\sim Exp(\alpha)$ oraz $T'\sim Exp(\alpha')$.
Zmienna będąca minimum zmiennych o rozkładach wykładniczych 
ma rozkład wykładniczy (zobacz np. \cite{probability-and-computing}), zatem
zmienna $\min(T, T')$ opisująca tempo wydobywania bloków przez całą sieć (ang. total hash rate) ma rozkład wykładniczy z~parametrem $\alpha + \alpha'$. 
Łatwo pokazać (zobacz np. \cite{probability-and-computing}), że: 
$$\mathbb{P}[T' < T] = \frac{\alpha'}{\alpha + \alpha'}~.$$
Zatem prawdopodobieństwa wydobycia kolejnego bloku odpowiednio przez adwersarza
i~przez uczciwe węzły są związane z tempem wydobywania bloków (ang. hash rate) i~wynoszą odpowiednio:
$$q = \frac{\alpha'}{\alpha + \alpha'}~~,$$
$$ p = 1- q = \frac{\alpha}{\alpha + \alpha'}~.$$

Dla~przykładu w systemie ~Bitcoin trudność puzzli obliczeniowych jest dostosowywana tak, aby średni czas znalezienia nowego bloku $\tau_0 = \frac{1}{\alpha + \alpha'}$ wynosił 10 minut. Stąd oczekiwane czasy wydobycia bloku przez uczciwe węzły i~adwersarza wynoszą odpowiednio:
$$\mathbb{E}[T] = \frac{1}{\alpha} = \frac{\tau_0}{p}~,$$
$$\mathbb{E}[T'] = \frac{1}{\alpha'} = \frac{\tau_0}{q}~.$$




Przyjmujemy, że~adwersarz posiada część $0 < q < \frac{1}{2}$ całkowitej mocy obliczeniowej sieci, natomiast pozostałe węzły mają do~dyspozycji część $p = 1 - q$, w~związku z tym prawdopodobieństwa wykopania kolejnego bloku wynoszą odpowiednio $q$  oraz $p$. Przeanalizujemy sytuację, w~której adwersarzowi brakuje $n$ bloków do zrównania się z~aktualnie najdłuższą gałęzią łańcucha.
Analiza jest podobna do analizy klasycznego problemu ruiny gracza (zobacz np. \cite{probability-and-computing}):

\begin{lemma}
Niech $q_n$ będzie prawdopodobieństwem zajścia zdarzenia $E_n$, które~odpowiada zrównaniu się aktualnie  najdłuższej gałęzi łańcucha i~gałęzi adwersarza, w~sytuacji kiedy adwersarzowi do zrównania się brakuje $n$ bloków. Wówczas $$\mathbb{P}[E_n] = q_n = \left(\frac{q}{p}\right)^n~.$$
\end{lemma}

\begin{proof}
Zauważmy, że $q_0 = 1$, $q_1 = \frac{q}{p}$.
W oparciu o własność Markowa możemy napisać:
$$q_{n+m} = \mathbb{P}[E_{n+m}] = \mathbb{P}[E_n|E_m] \cdot \mathbb{P}[E_m] = \mathbb{P}[E_n] \cdot \mathbb{P}[E_m] = q_n \cdot q_m~.$$ Z powyższego wynika, iż $q_n = q_1^n = (\frac{q}{p})^n$.
\end{proof}



\section{Analiza Nakamoto}
W~tej sekcji zaprezentujemy nieco uproszczoną analizę ataku double spend przedstawioną przez Satoshiego Nakamoto w~pracy \cite{bitcoin-invention}. 
Załóżmy, że uczciwe węzły nadbudowały wyjściowy blok $n$ nowymi blokami\footnote{W przypadku systemu Bitcoin przyjmuje się $n=6$, czyli po nadbudowaniu bloku zawierającego transakcję użytkownika przez $6$ bloków, użytkownik może uważać transakcję za potwierdzoną.},
a adwersarzowi w tym czasie udało się wydobyć pewną, nieznaną potencjalnej ofierze ataku, liczbę bloków $k$.
\mbox{Jeżeli $k \geq n$,} to adwersarzowi udało się dogonić łańcuch budowany przez uczciwe węzły i ma duże szansę przeprowadzenia ataku.
W~przeciwnym przypadku prawdopodobieństwo dogonienia uczciwych węzłów przez~adwersarza wynosi $(\frac{q}{p})^{n-k}$, co~pokazaliśmy w~poprzedniej sekcji.  Zatem możemy przedstawić prawdopodobieństwo $P(n)$, że adwersarzowi uda się dogonić uczciwe węzły jako: 
$$P(n) = \mathbb{P}[N'(S_n) \geq n] + \sum_{k = 0}^{n-1} \mathbb{P}[N'(S_n) = k] \cdot q_{n-k}~.$$
W dalszej części analizy Nakamoto dokonuje pewnego uproszczenia, zakładając, że faktyczny czas wydobywania bloków przez uczciwe węzły jest równy wartości oczekiwanej czasu wydobycia tych bloków. 
 Innymi słowy Nakamoto przybliża $N'(S_n)$ za~pomocą $N'(\mathbb{E}[S_n])$, gdzie: $$\mathbb{E}[S_n] = n \cdot \mathbb{E}[T] = n \cdot \frac{\tau_0}{p}~.$$
Z sekcji \ref{block-mining-analysis}, wiemy że~zmienną losową $N'(\mathbb{E}[S_n])$ można opisać za pomocą rozkładu Poissona z~parametrem $\lambda$, gdzie: $$\lambda = \alpha' \cdot \mathbb{E}[S_n] = \alpha'n \cdot \frac{\tau_0}{p} = n \cdot \frac{\alpha'}{\alpha + \alpha'} \cdot \frac{1}{p} = n \cdot \frac{q}{p}~.$$
Według Nakamoto mamy zatem: $$P(n) = \mathbb{P}[N'(\mathbb{E}[S_n]) \geq n] + \sum_{k=0}^{n-1} \mathbb{P}[N'(\mathbb{E}[S_n]) = k] \cdot q_{n-k}$$
$$= 1 - \sum_{k=0}^{n-1} \mathbb{P}[N'(\mathbb{E}[S_n]) = k] + \sum_{k=0}^{n-1} \mathbb{P}[N'(\mathbb{E}[S_n]) = k] \cdot q_{n-k}$$
$$= 1 - \sum_{k=0}^{n-1} \mathbb{P}[N'(\mathbb{E}[S_n]) = k](1 - q_{n-k})$$
$$= 1 - \sum_{k=0}^{n-1} e^{-\lambda} \frac{\lambda^k}{k!}(1 - q_{n-k})~.$$
Podkreślmy, że powyższa  analiza nie jest precyzyjna, gdyż opiera się na założeniu, iż $N'(S_n) = N'(\mathbb{E}[S_n])$.


\section{Analiza Grunspan'a}
W tej sekcji przedstawimy analizę ataku double spend, którą Grunspan et al.  przeprowadzili w~pracy \cite{double-spend-races}. Będziemy stosować oznaczenia z poprzednich sekcji. Zaczniemy od następującego twierdzenia.

\begin{theorem} 
\label{thm:dwumianowy}
Niech $X_n$ oznacza zmienną losową opisującą liczbę bloków wydobytych przez adwersarza w~chwili gdy~uczciwe węzły nadbudowały blok wyjściowy $n$ blokami.
Wówczas dla~$k \geq 0$ mamy: $$\mathbb{P}[X_n = k] = p^{n}q^{k}\binom{k+n-1}{k}~,$$
czyli zmienna $X_n$ ma rozkład ujemny dwumianowy $X_n \sim\mathrm{NB}(n, p)$.
\end{theorem}

\begin{proof}
Niech $k \geq 0$. Zauważmy, że~ zmienne $N'$ oraz~$S_n$ są~niezależne. Mamy zatem: $$\mathbb{P}[X_n = k] = \int_{0}^{+\infty} \mathbb{P}[N'(t) = k] \cdot f_{S_n}(t)dt$$
$$= \int_{0}^{+\infty} \frac{(\alpha't)^k}{k!} e^{-\alpha't} \cdot \frac{\alpha^n}{(n-1)!} t^{n-1}e^{-\alpha t} dt$$
$$= \frac{\alpha^n(\alpha')^k}{(n-1)!k!} \cdot \int_{0}^{+\infty} t^{k+n-1}e^{-t(\alpha'+\alpha)} dt$$
$$= \frac{p^{n}q^{k}}{(\tau_0)^{n+k}(n-1)!k!} \cdot \int_{0}^{+\infty} t^{k+n-1}e^{-\frac{t}{\tau_0}} dt~\bigg|_{\frac{dt}{\tau_0} = dz}^{\frac{t}{\tau_0} = z} \bigg|$$
$$= \frac{p^{n}q^{k}}{(\tau_0)^{n+k}(n-1)!k!} \cdot \int_{0}^{+\infty} (\tau_0)^{k+n-1} z^{k+n-1}e^{-z} \tau_0dz$$
$$= \frac{p^{n}q^{k}}{(\tau_0)^{n+k}(n-1)!k!} \cdot (\tau_0)^{k+n} \cdot \int_{0}^{+\infty} z^{k+n-1}e^{-z} dz$$
$$= \frac{p^{n}q^{k}}{(n-1)!k!} \cdot \int_{0}^{+\infty} z^{k+n-1}e^{-z} dz$$
$$= \frac{p^{n}q^{k}}{(n-1)!k!} \cdot (k+n-1)!$$
$$= p^{n}q^{k}\binom{k+n-1}{k}~.$$
\end{proof}
W kontekście analizy Nakamoto, możemy zauważyć, że~rozkład zmiennej $X_n$ zbiega do rozkładu Poissona.

\begin{theorem}
Niech $n \rightarrow \infty$ oraz niech $q \rightarrow 0$ i $l_n = n \cdot \frac{q}{p} \rightarrow \lambda$. Wówczas: $$\mathbb{P}[X_n = k] \rightarrow \frac{\lambda^k}{k!}e^{-\lambda}~.$$
\end{theorem}

\begin{proof}
$$\mathbb{P}[X_n = k] = \frac{n^n}{(n+l_n)^n} \cdot \frac{l_n^k}{(n+l_n)^k} \cdot \frac{(k+n-1)!}{(n-1)!k!}  = \frac{l_n^k}{k!} \cdot \frac{1}{(1 + \frac{l_n}{n})^n} \cdot \frac{n(n+1) \ldots (n+k-1)}{(n + l_n)^k}~.$$
Po~skorzystaniu z~faktu, że~$(1 + \frac{l_n}{n})^n \rightarrow e^{\lambda}$ otrzymujemy wynik.
\end{proof}

W oparciu o Twierdzenie \ref{thm:dwumianowy} możemy wyznaczyć prawdopodobieństwo $P(n)$ powodzenia ataku double spend, w sytuacji kiedy uczciwe węzły nadbudowały wyjściowy blok $n$ blokami. Innymi słowy, szukamy prawdopodobieństwa zdarzenia, że~adwersarz zbuduje  łańcuch dłuższy niż ten budowany przez uczciwe węzły.

\begin{theorem}
Prawdopodobieństwo sukcesu ataku double spend w~sytuacji, gdy uczciwe węzły wydobyły $n$ bloków wynosi: 
$$P(n) = 1 - \sum_{k=0}^{n-1} (p^{n}q^{k} - q^{n}p^{k}) \binom{k+n-1}{k} ~.$$
\end{theorem}

\begin{proof}
Dowód powyższego twierdzenia Grunspana et al. przebiega w dużym stopniu analogicznie do~analizy przedstawionej przez~Nakamoto:
$$P(n) = \sum_{k>n} p^{n}q^{k} \binom{k+n-1}{k} + \sum_{k=0}^{n} \left(\frac{q}{p}\right)^{n-k} p^{n}q^{k} \binom{k+n-1}{k}$$
$$= 1- \sum_{k=0}^{n} p^{n}q^{k} \binom{k+n-1}{k} + \sum_{k=0}^{n} \left(\frac{q}{p}\right)^{n-k} p^{n}q^{k} \binom{k+n-1}{k}$$
$$= 1- \sum_{k=0}^{n} p^{n}q^{k} \binom{k+n-1}{k} (1 - q^{n-k} p^{k-n})$$
$$= 1- \sum_{k=0}^{n} (p^{n}q^{k} - q^{k} q^{n-k} p^{n} p^{k-n}) \binom{k+n-1}{k}$$
$$= 1 - \sum_{k=0}^{n} (p^{n}q^{k} - q^{n}p^{k}) \binom{k+n-1}{k}$$
$$= 1 - \sum_{k=0}^{n-1} (p^{n}q^{k} - q^{n}p^{k}) \binom{k+n-1}{k}~.$$
\end{proof}

Wykorzystując  unormowaną niekompletną funkcję beta $I_x\left(a,b\right)$ (zobacz np. \cite{mathematical-functions-handbook}) Grunspan et al.
dowiedli również następującego twierdzenia determinującego asymptotyczne
zachowanie funkcji $P(n)$.


\begin{theorem} \label{closed-formula}
Dla $s = 4pq < 1$  oraz $n \rightarrow \infty$ mamy
 $$P(n) = I_s\left(n, \frac{1}{2}\right)\sim \frac{s^n}{\sqrt{\pi(1-s)n}}~.$$
\end{theorem}

Dowód twierdzenia \ref{closed-formula} znajduje się w pracy \cite{double-spend-races}.  Nakamoto postulował, że~$P(n)$ w tempie wykładniczym maleje do~0, gdy~$n \rightarrow \infty$, jego rozumowanie nie było jednak w pełni formalne.
Grunspan et al. potwierdzili postulat Nakamoto.
